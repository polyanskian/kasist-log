<?php

namespace app\models;

use app\DbTableName;
use Yii;

/**
 * This is the model class for table "request_personaldata".
 *
 * @property int $id
 * @property int $import_id Импорт
 * @property int|null $ip_id
 * @property string|null $ip_unknown Неизвестный IP
 *
 * @property LogImport $import
 * @property Ip $ip
 */
class Personaldata extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return DbTableName::REQUEST_PERSONALDATA;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['import_id'], 'required'],
            [['import_id', 'ip_id'], 'integer'],
            [['ip_unknown'], 'string', 'max' => 16],
            [['import_id'], 'exist', 'skipOnError' => true, 'targetClass' => LogImport::className(), 'targetAttribute' => ['import_id' => 'id']],
            [['ip_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ip::className(), 'targetAttribute' => ['ip_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'import_id' => 'Импорт',
            'ip_id' => 'Ip ID',
            'ip_unknown' => 'Неизвестный IP',
        ];
    }

    /**
     * Gets query for [[Import]].
     *
     * @return \yii\db\ActiveQuery|\app\models\query\LogImportQuery
     */
    public function getImport()
    {
        return $this->hasOne(LogImport::className(), ['id' => 'import_id'])->inverseOf('personaldatas');
    }

    /**
     * Gets query for [[Ip]].
     *
     * @return \yii\db\ActiveQuery|\app\models\query\IpQuery
     */
    public function getIp()
    {
        return $this->hasOne(Ip::className(), ['id' => 'ip_id'])->inverseOf('personaldatas');
    }

    /**
     * {@inheritdoc}
     * @return \app\models\query\PersonaldataQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\PersonaldataQuery(get_called_class());
    }
}
