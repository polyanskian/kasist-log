<?php

namespace app\models;

use app\DbTableName;

/**
 * This is the model class for table "log_dir".
 *
 * @property int $id
 * @property string $name Имя
 * @property string $directory Директория
 *
 * @property LogImport[] $logImports
 */
class LogDir extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return DbTableName::LOG_DIRECTORY;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'directory'], 'required'],
            [['name', 'directory'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['directory'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'directory' => 'Директория',
        ];
    }

    /**
     * Gets query for [[LogImports]].
     *
     * @return \yii\db\ActiveQuery|\app\models\query\LogImportQuery
     */
    public function getLogImports()
    {
        return $this->hasMany(LogImport::className(), ['log_directory_id' => 'id'])->inverseOf('logDirectory');
    }

    /**
     * {@inheritdoc}
     * @return \app\models\query\LogDirQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\LogDirQuery(static::class);
    }
}
