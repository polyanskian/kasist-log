<?php

namespace app\models;

use app\DbTableName;
use Yii;

/**
 * @property int $id
 * @property int $log_directory_id
 * @property string $file_name Имя файла
 * @property string $date Дата
 * @property int $is_completed Завершено
 * @property int $is_run Выполняется
 * @property string|null $date_update Дата импорта
 *
 * @property LogDir $logDirectory
 * @property SearchText[] $statApiSearchTexts
 * @property ServerInfo[] $statApiServerInfoKassists
 */
class LogImport extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return DbTableName::LOG_IMPORT;
    }

    public function rules()
    {
        $formatter = \Yii::$app->formatter;

        return [
            [['log_directory_id', 'file_name', 'date'], 'required'],
            [['log_directory_id'], 'integer'],
            [['is_completed', 'is_run'], 'boolean'],
            [['file_name'], 'string', 'max' => 255],

            [['date'], 'filter', 'filter' => function($value) use ($formatter) {
                return $formatter->asDbDate($value);
            }, 'skipOnEmpty' => true],
            [['date'], 'date', 'format' => "php:$formatter->dbDateFormat"],

            [['date_update'], 'filter', 'filter' => function($value) use ($formatter) {
                return $formatter->asDbDateTime($value);
            }, 'skipOnEmpty' => true],
            [['date_update'], 'datetime', 'format' => "php:$formatter->dbDateTimeFormat", 'skipOnEmpty' => true],

            [['file_name', 'log_directory_id'], 'unique', 'targetAttribute' => ['file_name', 'log_directory_id']],
            [['log_directory_id'], 'exist', 'skipOnError' => true, 'targetClass' => LogDir::class, 'targetAttribute' => ['log_directory_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'log_directory_id' => 'Log Directory ID',
            'file_name' => 'Имя файла',
            'date' => 'Дата',
            'is_completed' => 'Завершено',
            'is_run' => 'Выполняется',
            'date_update' => 'Дата импорта',
        ];
    }

    public function getFileRelativePath(): string
    {
        return "{$this->logDirectory->directory}/$this->file_name";
    }

    /**
     * Gets query for [[LogDirectory]].
     *
     * @return \yii\db\ActiveQuery|\app\models\query\LogDirQuery
     */
    public function getLogDirectory()
    {
        return $this->hasOne(LogDir::className(), ['id' => 'log_directory_id'])->inverseOf('logImports');
    }

    /**
     * Gets query for [[StatApiSearchTexts]].
     *
     * @return \yii\db\ActiveQuery|\app\models\query\SearchTextQuery
     */
    public function getStatApiSearchTexts()
    {
        return $this->hasMany(SearchText::className(), ['import_id' => 'id'])->inverseOf('import');
    }

    /**
     * Gets query for [[StatApiServerInfoKassists]].
     *
     * @return \yii\db\ActiveQuery|\app\models\query\ServerInfoQuery
     */
    public function getStatApiServerInfoKassists()
    {
        return $this->hasMany(ServerInfo::className(), ['import_id' => 'id'])->inverseOf('import');
    }

    /**
     * {@inheritdoc}
     * @return \app\models\query\LogImportQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\LogImportQuery(static::class);
    }
}
