<?php

namespace app\models;

use app\DbTableName;
use Yii;

/**
 * This is the model class for table "api_search_text".
 *
 * @property int $id
 * @property int $import_id Импорт
 * @property int|null $ip_id
 * @property string|null $ip_unknown Неизвестный IP
 * @property string|null $soft Программа
 * @property string $date Дата запроса
 *
 * @property LogImport $import
 * @property Ip $ip
 */
class SearchText extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return DbTableName::REQUEST_SEARCH_TEXT;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['import_id', 'date'], 'required'],
            [['import_id', 'ip_id'], 'integer'],
            [['date'], 'safe'],
            [['ip_unknown'], 'string', 'max' => 16],
            [['soft'], 'string', 'max' => 255],
            [['import_id'], 'exist', 'skipOnError' => true, 'targetClass' => LogImport::className(), 'targetAttribute' => ['import_id' => 'id']],
            [['ip_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ip::className(), 'targetAttribute' => ['ip_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'import_id' => 'Импорт',
            'ip_id' => 'Ip ID',
            'ip_unknown' => 'Неизвестный IP',
            'soft' => 'Программа',
            'date' => 'Дата запроса',
        ];
    }

    /**
     * Gets query for [[Import]].
     *
     * @return \yii\db\ActiveQuery|\app\models\query\LogImportQuery
     */
    public function getImport()
    {
        return $this->hasOne(LogImport::className(), ['id' => 'import_id'])->inverseOf('apiSearchTexts');
    }

    /**
     * Gets query for [[Ip]].
     *
     * @return \yii\db\ActiveQuery|\app\models\query\IpQuery
     */
    public function getIp()
    {
        return $this->hasOne(Ip::className(), ['id' => 'ip_id'])->inverseOf('apiSearchTexts');
    }

    /**
     * {@inheritdoc}
     * @return \app\models\query\SearchTextQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\SearchTextQuery(get_called_class());
    }
}
