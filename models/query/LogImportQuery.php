<?php

namespace app\models\query;

use yii\db\ActiveQuery;

/**
 * @see \app\models\LogImport
 */
class LogImportQuery extends ActiveQuery
{
    public function byId(int $id): self
    {
        return $this->andWhere(['id' => $id]);
    }
}
