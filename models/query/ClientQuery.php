<?php

namespace app\models\query;

use yii\db\ActiveQuery;

/**
 * @see \app\models\Client
 */
class ClientQuery extends ActiveQuery
{
    public function byLogins(array $logins): self
    {
        return $this->andWhere(['login' => $logins]);
    }

    public function byEmptyName(): self
    {
        return $this->andWhere(['or',
            ['name' => ''],
            ['name' => null]
        ]);
    }
}
