<?php

namespace app\models\query;

/**
 * This is the ActiveQuery class for [[\app\models\ApiDocInfoNd]].
 *
 * @see \app\models\DocInfoNd
 */
class DocInfoNdQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \app\models\DocInfoNd[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\DocInfoNd|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function byImportId(int $importId): self
    {
        return $this->andWhere(['import_id' => $importId]);
    }
}
