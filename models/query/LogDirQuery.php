<?php

namespace app\models\query;

use yii\db\ActiveQuery;

/**
 * @see \app\models\LogDir
 */
class LogDirQuery extends ActiveQuery
{
    public function byId(int $id): self
    {
        return $this->andWhere(['id' => $id]);
    }
}
