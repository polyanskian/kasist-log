<?php

namespace app\models\query;

/**
 * This is the ActiveQuery class for [[\app\models\ApiServerInfoKassist]].
 *
 * @see \app\models\ServerInfo
 */
class ServerInfoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \app\models\ServerInfo[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\ServerInfo|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function byImportId(int $importId): self
    {
        return $this->andWhere(['import_id' => $importId]);
    }
}
