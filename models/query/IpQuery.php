<?php

namespace app\models\query;

use yii\db\ActiveQuery;

/**
 * @see \app\models\Ip
 */
class IpQuery extends ActiveQuery
{
    public function byIps(array $ips): self
    {
        return $this->andWhere(['ip' => $ips]);
    }
}
