<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "client".
 *
 * @property int $id
 * @property string $login Логин
 * @property string|null $name Компания
 *
 * @property ClientToIp[] $clientToIps
 * @property Ip[] $ips
 */
class Client extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'client';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['login'], 'required'],
            [['login', 'name'], 'string', 'max' => 255],
            [['login'], 'unique'],
            [['login', 'name'], 'unique', 'targetAttribute' => ['login', 'name']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'login' => 'Логин',
            'name' => 'Компания',
        ];
    }

    /**
     * Gets query for [[ClientToIps]].
     *
     * @return \yii\db\ActiveQuery|\app\models\query\ClientToIpQuery
     */
    public function getClientToIps()
    {
        return $this->hasMany(ClientToIp::className(), ['client_id' => 'id'])->inverseOf('client');
    }

    /**
     * Gets query for [[Ips]].
     *
     * @return \yii\db\ActiveQuery|\app\models\query\IpQuery
     */
    public function getIps()
    {
        return $this->hasMany(Ip::className(), ['id' => 'ip_id'])->viaTable('client_to_ip', ['client_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\query\ClientQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\ClientQuery(get_called_class());
    }
}
