<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ip".
 *
 * @property int $id
 * @property string $ip
 *
 * @property ClientToIp[] $clientToIps
 * @property Client[] $clients
 * @property RequestLog[] $requestLogs
 */
class Ip extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ip';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ip'], 'required'],
            [['ip'], 'string', 'max' => 16],
            [['ip'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ip' => 'Ip',
        ];
    }

    /**
     * Gets query for [[ClientToIps]].
     *
     * @return \yii\db\ActiveQuery|\app\models\query\ClientToIpQuery
     */
    public function getClientToIps()
    {
        return $this->hasMany(ClientToIp::className(), ['ip_id' => 'id'])->inverseOf('ip');
    }

    /**
     * Gets query for [[Clients]].
     *
     * @return \yii\db\ActiveQuery|\app\models\query\ClientQuery
     */
    public function getClients()
    {
        return $this->hasMany(Client::className(), ['id' => 'client_id'])->viaTable('client_to_ip', ['ip_id' => 'id']);
    }

    /**
     * Gets query for [[RequestLogs]].
     *
     * @return \yii\db\ActiveQuery|\app\models\query\RequestLogQuery
     */
    public function getRequestLogs()
    {
        return $this->hasMany(RequestLog::className(), ['ip_id' => 'id'])->inverseOf('ip');
    }

    /**
     * {@inheritdoc}
     * @return \app\models\query\IpQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\IpQuery(get_called_class());
    }
}
