<?php

namespace app\models\search;

use app\models\LogDir;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LogImport;

class LogImportSearch extends LogImport
{
    public $directory = '';

    public function rules()
    {
        return [
            [['id', 'log_directory_id'], 'integer'],
            [['file_name', 'date_update', 'directory'], 'safe'],
            [['is_completed', 'is_run'], 'boolean'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search(array $params): ActiveDataProvider
    {
        $query = LogImport::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $taleImport = LogImport::tableName();
        $taleDir = LogDir::tableName();

        $query->joinWith('logDirectory');

        $query->andFilterWhere([
            "$taleImport.id" => $this->id,
            'is_completed' => $this->is_completed,
            'is_run' => $this->is_run,
        ]);

        $query->andFilterWhere(['like', 'file_name', $this->file_name]);
        $query->andFilterWhere(['like', 'directory', $this->directory]);
        $query->andFilterWhere(['like', 'date_update', $this->date_update]);

        $dataProvider->sort->attributes['directory'] = [
            'asc' => ["$taleDir.directory" => SORT_ASC],
            'desc' => ["$taleDir.directory" => SORT_DESC],
        ];

        return $dataProvider;
    }
}
