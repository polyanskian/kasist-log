<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Client;
use yii\db\ActiveQuery;

class ClientSearch extends Client
{
    public string $ip = '';

    public function rules()
    {
        return [
            [['login', 'name', 'ip'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search(array $params): ActiveDataProvider
    {
        $query = Client::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'login' => SORT_ASC,
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->joinWith([
            'ips' => function (ActiveQuery $query) {
                $query->orderBy(['ip' => SORT_ASC]);
            },
        ]);

        $query
            ->andFilterWhere(['like', 'login', $this->login])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'ip.ip', $this->ip]);

        $dataProvider->sort->attributes['ip'] = [
            'asc' => ['ip.ip' => SORT_ASC],
            'desc' => ['ip.ip' => SORT_DESC],
        ];

        return $dataProvider;
    }
}
