<?php

/**
 * @var $model \yii\db\ActiveRecordInterface
 */

use yii\helpers\Html;

?>

<?php if (!$model->getIsNewRecord()) : ?>
    <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data-method' => 'POST',
        'data-confirm' => "Удалить?",
    ]) ?>

    <?= Html::a('Создать', ['create'], ['class' => 'btn btn-outline-primary']) ?>
<?php endif ?>

<?= Html::a('Отмена', ['index'], ['class' => 'btn btn-outline-secondary']) ?>

<?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
