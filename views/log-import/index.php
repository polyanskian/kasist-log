<?php

/**
 * @var $this yii\web\View
 * @var $searchModel app\models\search\LogImportSearch
 * @var $dataProvider yii\data\ActiveDataProvider
 */

use app\controllers\LogImportController;
use app\GridView\Column\BooleanDataColumn;
use app\models\LogDir;
use app\models\LogImport;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

$this->title = LogImportController::TITLE_INDEX;
$this->params['breadcrumbs'][] = $this->title;

?>

<h1><?= Html::encode($this->title) ?></h1>

<p class="text-right">
    <?= $this->render('/_parts/button/button-clear-filter') ?>
    <?= Html::a('Создать импорт', ['create'], ['class' => 'btn btn-primary']) ?>
</p>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
           'attribute' => 'id',
           'headerOptions' => [
               'style' => 'width: 100px',
           ]
        ],
//        'log_directory_id',
        [
            'attribute' => 'directory',
            'label' => 'Директория',
            'value' => function(LogImport $model) {
                return $model->logDirectory->directory;
            }
        ],
        'file_name',
//        [
//            'attribute' => 'file_name',
//            'format' => 'raw',
//            'value' => function(RequestImport $model) {
//                return Html::a($model->file_name, ['update', 'id' => $model->id]);
//            },
//        ],
        [
            'attribute' => 'is_completed',
            'class' => BooleanDataColumn::class
        ],
        [
            'attribute' => 'is_run',
            'class' => BooleanDataColumn::class
        ],
        [
            'attribute' => 'date_update',
            'headerOptions' => [
                'style' => 'width: 180px',
            ],
        ],
        [
            'class' => ActionColumn::class,
            'template' => '{restart} {delete}',
//            'urlCreator' => function ($action, RequestImport $model, $key, $index, $column) {
//                return Url::toRoute([$action, 'id' => $model->id]);
//             },
            'buttons' => [
                'restart' => function(string $url, LogImport $model, int $key) {
                    return Html::a('&#8635;', $url, [
                        'style' => 'font-weight: bold; font-size: 1.3rem; line-height: 0;',
                        'data-method' => 'POST',
                        'data-confirm' => "Перезапустить???",
                        'title' => 'Перезапустить',
                    ]);
                }
            ],
        ],
    ],
]) ?>
