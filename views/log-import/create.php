<?php

/**
 * @var $this View
 * @var $modelShowFile FindLogModel
 * @var $modelFile FileLogModel
 * @var $logFileNames string[]
 * @var $logDirs LogDir[]
 */

use app\models\LogDir;
use app\Service\LogImport\Model\FileLogModel;
use app\Service\LogImport\Model\FindLogModel;
use yii\helpers\Html;
use yii\web\View;

$this->title = 'Создать импорт';
$this->params['breadcrumbs'][] = ['label' => 'Импорт', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

//echo "<pre>".print_r(Yii::$app->request->post(), true).'</pre>';
//echo "<pre>".print_r($model->toArray(), true).'</pre>';

?>

<h1><?= Html::encode($this->title) ?></h1>

<?= $this->render('_form-create', [
    'modelShowFile' => $modelShowFile,
    'modelFile' => $modelFile,
    'logDirs' => $logDirs,
    'logFileNames' => $logFileNames,
]) ?>

<?php // $form->field($model, 'is_started')->checkbox() ?>
<?php // $form->field($model, 'date_complete')->textInput() ?>

<div class="form-group">
    <?php // Html::a('Отмена', ['index'], ['class' => 'btn btn-outline-secondary']) ?>
    <?php // Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
</div>
