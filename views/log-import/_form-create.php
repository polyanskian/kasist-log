<?php

/**
 * @var $this \yii\web\View
 * @var $modelShowFile FindLogModel
 * @var $modelFile FileLogModel
 * @var $logFileNames string[]
 * @var $logDirs LogDir[]
 */

use app\models\LogDir;
use app\Service\LogImport\Model\FileLogModel;
use app\Service\LogImport\Model\FindLogModel;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;

$countLogFileNames = count($logFileNames);
$selectLogFileNames = array_combine($logFileNames, $logFileNames);
$selectLogDirs = array_combine(array_column($logDirs, 'directory'), array_column($logDirs, 'name'));

?>

<?php $form = ActiveForm::begin([
    'action' => ['create'],
    'method' => 'POST',
    'options' => [
        'autocomplete' => 'off',
    ],
]); ?>
    <div class="text-right mb-3">
        <?= $this->render('/_parts/button/button-cancel') ?>

        <?php echo Html::submitButton('Получить список файлов', [
            'class' => 'btn btn-primary',
        ]) ?>

        <?php if ($logFileNames) : ?>
            <?php echo Html::submitButton('Создать импорт', [
                'class' => 'btn btn-success',
                'name' => Html::getInputName($modelFile, 'isActionCreate'),
                'value' => 1,
                'data-confirm' => "Создать импорт?",
            ]) ?>
        <?php endif ?>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($modelShowFile, 'dateFrom')->textInput(['type' => 'date']) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($modelShowFile, 'dateTo')->textInput(['type' => 'date']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($modelFile, 'files', [
                'template' => "{label}: $countLogFileNames\n{input}\n{hint}\n{error}",
            ])->dropdownList($selectLogFileNames, [
                'multiple' => true,
                'size' => 20,
            ])
            ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($modelShowFile, 'dirs')->dropdownList($selectLogDirs, [
                'multiple' => true,
                'size' => count($selectLogDirs),
            ]) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>
