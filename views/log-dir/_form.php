<?php

/**
 * @var $this yii\web\View
 * @var $model app\models\LogDir
 * @var $form yii\widgets\ActiveForm
 * @var $logDirPaths string[]
 */

use yii\widgets\ActiveForm;

$selectLogDirPaths = array_combine($logDirPaths, $logDirPaths);

?>

<?php $form = ActiveForm::begin() ?>
    <p class="text-right">
        <?= $this->render('/_parts/button/button-delete-create-cancel-save', [
            'model' => $model,
        ]) ?>
    </p>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'directory')->dropDownList($selectLogDirPaths, [
        'disabled' => !$model->getIsNewRecord(),
    ]) ?>
<?php ActiveForm::end() ?>
