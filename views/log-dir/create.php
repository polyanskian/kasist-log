<?php

/**
 * @var $logDirPaths string[]
 * @var $this yii\web\View
 * @var $model app\models\LogDir
 */

use app\controllers\LogDirController;
use yii\helpers\Html;

$this->title = 'Создать директорию';
$this->params['breadcrumbs'][] = ['label' => LogDirController::TITLE_INDEX, 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Создать';

?>

<h1><?= Html::encode($this->title) ?></h1>

<?= $this->render('_form', [
    'model' => $model,
    'logDirPaths' => $logDirPaths,
]) ?>
