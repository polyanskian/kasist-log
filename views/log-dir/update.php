<?php

/**
 * @var $this yii\web\View
 * @var $model app\models\LogDir
 * @var $logDirPaths string[]
 */

use app\controllers\LogDirController;
use yii\helpers\Html;

$this->title = "Редактировать директорию: $model->name";
$this->params['breadcrumbs'][] = ['label' => LogDirController::TITLE_INDEX, 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->name;

?>

<h1><?= Html::encode($this->title) ?></h1>

<?= $this->render('_form', [
    'model' => $model,
    'logDirPaths' => $logDirPaths,
]) ?>

