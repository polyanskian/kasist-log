<?php

/**
 * @var $this yii\web\View
 * @var $searchModel app\models\search\LogDirSearch
 * @var $dataProvider yii\data\ActiveDataProvider
 */

use app\controllers\LogDirController;
use app\models\LogDir;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;


$this->title = LogDirController::TITLE_INDEX;
$this->params['breadcrumbs'][] = $this->title;

?>

<h1><?= Html::encode($this->title) ?></h1>

<p class="text-right">
    <?= $this->render('/_parts/button/button-clear-filter') ?>
    <?= Html::a('Создать директорию', ['create'], ['class' => 'btn btn-primary']) ?>
</p>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

//        'id',
        [
            'attribute' => 'name',
            'format' => 'raw',
            'value' => function(LogDir $model) {
                return Html::a($model->name, ['update', 'id' => $model->id]);
            },
        ],
        'directory',
        [
            'class' => ActionColumn::class,
            'template' => '{delete}',
//            'urlCreator' => function ($action, RequestDir $model) {
//                return Url::toRoute([$action, 'id' => $model->id]);
//             }
        ],
    ],
]) ?>
