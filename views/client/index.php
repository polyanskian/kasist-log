<?php

/**
 * @var $this yii\web\View
 * @var $searchModel app\models\search\ClientSearch
 * @var $dataProvider yii\data\ActiveDataProvider
 */

use app\controllers\ClientController;
use app\models\Client;
use app\models\Ip;
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = ClientController::TITLE_INDEX;
$this->params['breadcrumbs'][] = $this->title;

?>

<h1><?= Html::encode($this->title) ?></h1>

<p class="text-right">
    <?= Html::a('Удалить всех клиентов', ['delete-all'], [
        'class' => 'btn btn-danger',
        'data-method' => 'POST',
        'data-confirm' => "Удалить???",
    ]) ?>

    <?= $this->render('/_parts/button/button-clear-filter') ?>

    <?= Html::a('Импортировать', ['import-start'], [
        'class' => 'btn btn-primary',
        'data-method' => 'POST',
        'data-confirm' => "Запустить импорт?",
    ]) ?>
</p>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'login',
        'name',
        [
            'attribute' => 'ip',
            'label' => 'IP',
            'format' => 'raw',
            'value' => function(Client $model) {
                $ips = array_map(function (Ip $model) {
                    return $model->ip;
                }, $model->ips);

                return '<pre>' . implode("\n", $ips) . '</pre>';
            },
        ],
    ],
]) ?>

