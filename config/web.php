<?php

return \yii\helpers\ArrayHelper::merge(
    require __DIR__ . '/common/common.php',
    require __DIR__ . '/common/di.php',
    require __DIR__ . '/common/di-web.php',
    require __DIR__ . '/web/web.php',
    require __DIR__ . '/db/db.php',
    require __DIR__ . '/common/debug.php',
    require __DIR__ . '/common/gii.php',
    require __DIR__ . '/common/queue.php',
    require __DIR__ . '/local/local.php'
);
