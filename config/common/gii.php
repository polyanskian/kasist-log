<?php

if (YII_ENV_DEV) {
    return [
        'bootstrap' => ['gii'],
        'modules' => [
            'gii' => [
                'class' => 'yii\gii\Module',
                'allowedIPs' => ['*'],
                // uncomment the following to add your IP if you are not connecting from localhost.
                //'allowedIPs' => ['127.0.0.1', '::1'],
                'generators' => [
                    'job' => [
                        'class' => \yii\queue\gii\Generator::class,
                    ],
                ],
            ]
        ]
    ];
}

return [];
