<?php

use app\Formatter;

return [
    'language' => 'ru-RU',
    'basePath' => dirname(__DIR__, 2),
    'aliases' => [
        '@runtime' => '@app/var',
    ],
//    'params' => require __DIR__ . '/params.php',
    'components' => [
        'formatter' => [
            'class' => Formatter::class,
        ],
    ],
];
