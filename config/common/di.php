<?php

use app\AppParams;
use app\Formatter;
use app\PathResolver;
use app\Service\Client\ClientService;
use app\Service\LogImport\LogImportService;
use app\Service\RequestLog\Parser\RequestLogFileParser;
use app\Xxx\Handler\DndHandler;
use app\Xxx\Handler\DocInfoNdHandler;
use app\Xxx\Handler\MainpageSourceHandler;
use app\Xxx\Handler\PersonaldataHandler;
use app\Xxx\Handler\SearchqHandler;
use app\Xxx\Handler\SearchTextHandler;
use app\Xxx\Handler\ServerInfoHandler;
use app\Xxx\PrepareLine\TrimPrepareLine;
use app\Xxx\PrepareLine\UrlDecodePrepareLine;
use app\Xxx\LogFileImporter;
use app\Xxx\Processor\DndProcessor;
use app\Xxx\Processor\DocInfoNdProcessor;
use app\Xxx\Processor\MainpageSourceProcessor;
use app\Xxx\Processor\PersonaldataProcessor;
use app\Xxx\Processor\SearchqProcessor;
use app\Xxx\Processor\SearchTextProcessor;
use app\Xxx\Processor\ServerInfoProcessor;
use yii\di\Container;
use yii\queue\Queue;

$dirBase = dirname(__DIR__, 2);

return [
    'container' => [
        'definitions' => [
            LogFileImporter::class => function(Container $c) {
                $prepareLineHandlers = [
                    new UrlDecodePrepareLine(),
                    new TrimPrepareLine(),
                ];

                $processors = [
                    new ServerInfoProcessor(new ServerInfoHandler()),
                    new SearchTextProcessor(new SearchTextHandler()),
                    new DocInfoNdProcessor(new DocInfoNdHandler()),
                    new MainpageSourceProcessor(new MainpageSourceHandler()),
                    new DndProcessor(new DndHandler()),
                    new PersonaldataProcessor(new PersonaldataHandler()),
                    new SearchqProcessor(new SearchqHandler()),
                ];

                return new LogFileImporter($processors, $prepareLineHandlers);
            },



//            LogFileRequestRepository::class => function(Container $c) {
//                return new LogFileRequestRepository(
//
//                    $c->get(PathResolver::class)
//                );
//            },

            RequestLogFileParser::class => function() {
                return new RequestLogFileParser(
                    '-- GET /.apiSearch?',
                    '#^\[([\d\- :]+)\]: ([\d.]+) -- GET /\.apiSearch.+/([Microsoft \w]+)/#si',
                );
            },

//            ImportClientCommandHandler::class => ImportClientCommandHandler::class,
//
//            ImportService::class => function(Container $c) use ($dirBase) {
//                return new ImportService(
//                    $dirBase,
//                    $c->get(LogFileRequestRepository::class),
//                    $c->get(DateTimeHelper::class),
//                    $c->get(RequestDataImporter::class),
//                );
//            },






//            ImportClientCommandHandler::class => [
//                'class' => ImportClientCommandHandler::class,
//                '__construct()' => [
//                    'dirBase' => $dirBase,
//                ]
//            ],

//            ClientRepository::class => function() {
//                $connection = Yii::$app->get('dbClient');
//                return new ClientRepository($connection);
//            },
//
//
//
//            RequestData::class => RequestData::class,
//


        ],
        'singletons' => [
            ClientService::class => ClientService::class,
            LogImportService::class => LogImportService::class,

            Formatter::class => Formatter::class,

            Queue::class => function() {
                return \Yii::$app->get('queue');
            },

            PathResolver::class => function() use ($dirBase) {
                return new PathResolver($dirBase);
            },

//            AppHelper::class => AppHelper::class,
            AppParams::class => function() {
                return new AppParams(require __DIR__ . '/params.php',);
            },
        ]
    ]
];
