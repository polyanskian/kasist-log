<?php

if (YII_ENV_DEV) {
    return [
        'bootstrap' => ['debug'],
        'modules' => [
            'debug' => [
                'class' => 'yii\debug\Module',
                'allowedIPs' => ['*'],
                // uncomment the following to add your IP if you are not connecting from localhost.
                //'allowedIPs' => ['127.0.0.1', '::1'],
//                'panels' => [
//                    'queue' => \yii\queue\debug\Panel::class,
//                ],
            ]
        ]
    ];
}

return [];
