<?php

$dirBase = dirname(__DIR__, 2);

return [
    'container' => [
        'definitions' => [
            \yii\widgets\ActiveForm::class => \yii\bootstrap4\ActiveForm::class,

            \yii\widgets\LinkPager::class => [
                'class' => \yii\bootstrap4\LinkPager::class,
                'nextPageLabel' => '&rsaquo;',
                'prevPageLabel' => '&lsaquo;',
                'firstPageLabel' => '&laquo;',
                'lastPageLabel'  => '&raquo;',
            ],

            \yii\grid\GridView::class => [
                'class' => \yii\grid\GridView::class,
                'layout' => '{summary}{items}<div class="d-flex justify-content-center">{pager}</div>',
            ]
        ],
        'singletons' => [
            \app\NotifyManager\NotifyManagerInterface::class => \app\NotifyManager\NotifyManager::class,
            \yii\web\Session::class => \yii\web\Session::class,
        ]
    ]
];
