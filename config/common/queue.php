<?php

return [
    'bootstrap' => ['queue'], // Компонент регистрирует свои консольные команды
    'components' => [
        'queue' => [
            'class' => \yii\queue\file\Queue::class,
            'path' => '@runtime/queue',
        ],
    ],
];
