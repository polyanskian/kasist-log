<?php

return [
    'dirRequestLog' => 'data/logs', // Директория с логами запросов
    'dataCompanyPath' => 'data/client/client_info.csv', // Данные о компаниях

    // Данные о клиентах (id = ip)
    'dbClientPaths' => [
        'data/client/reportUdata.sqlite',
    ],
];
