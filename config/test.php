<?php

return \yii\helpers\ArrayHelper::merge(
    require __DIR__ . '/common/common.php',
    require __DIR__ . '/common/di.php',
    require __DIR__ . '/test/test.php',
    require __DIR__ . '/db/db-test.php',
    require __DIR__ . '/local/local.php'
);
