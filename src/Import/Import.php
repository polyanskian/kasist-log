<?php

namespace app\Import;

use app\models\LogImport;

class Import
{
    private LogImport $logImportModel;

    public function __construct(LogImport $logImportModel)
    {
        if (!$logImportModel->id) {
            throw new \Exception('ID модели не задан');
        }

//        if ($logImportModel->is_run) {
//            throw new \Exception("Импорт уже запущен id=$logImportModel->id");
//        }

        $this->logImportModel = $logImportModel;
    }


}
