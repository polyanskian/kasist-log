<?php

namespace app\Service\LogImport\Command;

use app\models\LogImport;

class StartLogImportCommand
{
    private LogImport $model;

    public function __construct(LogImport $model)
    {
        $this->model = $model;
    }

    public function getModel(): LogImport
    {
        return $this->model;
    }

    public function validate(): array
    {
        $errors = [];

        if (!$this->model->id) {
            $errors[] = 'ID модели не задан';
        }

        if (!$this->model->is_completed) {
            $errors[] = 'Импорт уже запущен';
        }

        return $errors;
    }
}
