<?php

declare(strict_types=1);

namespace app\Service\LogImport;

use app\Exception\ShowUserException;
use app\Formatter;
use app\jobs\ImportRequestLogJob;
use app\models\LogDir;
use app\models\LogImport;
use app\models\RequestLog;
use app\PathResolver;
use app\Service\LogImport\Model\FileLogModel;
use app\Service\LogImport\Model\FindLogModel;
use app\Service\RequestLog\Import\RequestLogImporter;
use app\Service\RequestLog\RequestLogFileFinder;
use app\Xxx\LogFileImporter;
use yii\helpers\ArrayHelper;
use yii\queue\Queue;

class LogImportService
{
    private RequestLogFileFinder $logFileFinder;
    private Formatter $formatter;
    private Queue $queue;
    private RequestLogImporter $logImporter;
    private LogFileImporter $logHandleManager;
    private PathResolver $pathResolver;

    public function __construct(
        RequestLogFileFinder $logFileFinder,
        Formatter $formatter,
        Queue $queue,
        RequestLogImporter $logImporter,
        LogFileImporter $logHandleManager,
        PathResolver $pathResolver
    ) {
        $this->logFileFinder = $logFileFinder;
        $this->formatter = $formatter;
        $this->queue = $queue;
        $this->logImporter = $logImporter;
        $this->logHandleManager = $logHandleManager;
        $this->pathResolver = $pathResolver;
    }

    public function getImportLogFileNames(FindLogModel $findLogModel): array
    {
        $dateFrom = new \DateTime($findLogModel->dateFrom);
        $dateTo = new \DateTime($findLogModel->dateTo);
        $dirs = (array) $findLogModel->dirs;

        if (!$dirs) {
            throw new \Exception('Не задан параметр `dirs`');
        }

        try {
            return $this->logFileFinder->getLogFileNames($dateFrom, $dateTo, $dirs);
        } catch (\InvalidArgumentException $e) {
            throw new ShowUserException($e->getMessage(), 0, $e);
        }
    }

    /**
     * @param LogDir[] $logDirModels
     * @return LogImport[]
     */
    public function createImports(FileLogModel $fileLogModel, array $logDirModels): array
    {
        $logDirModels = ArrayHelper::index($logDirModels, 'directory');
        $importModels = [];

        foreach ($fileLogModel->files as $file) {
            $fileName = basename($file);
            $dir = dirname($file);
            $dirLogModel = $logDirModels[$dir] ?? null;
            $dateFile = $this->extractDateFromFileName($fileName);

            $logImportModel = new LogImport();
            $logImportModel->file_name = $fileName;
            $logImportModel->date = $dateFile;
            $logImportModel->is_completed = false;
            $logImportModel->is_run = false;
            $logImportModel->date_update = null;
            $logImportModel->link('logDirectory', $dirLogModel);

            $this->saveModel($logImportModel);

            $importModels[] = $logImportModel;
        }

        return $importModels;
    }

    public function runImport(LogImport $model): void
    {
        $model->is_completed = false;
        $this->saveModel($model);

        $job = new ImportRequestLogJob();
        $job->importId = $model->id;
        $this->queue->push($job);
    }

    public function restartImport(LogImport $model): void
    {
        $this->runImport($model);
    }

    public function processImport(LogImport $model): void
    {
        if ($model->is_run) {
            return;
        }

        $model->is_run = true;
        $model->is_completed = false;
        $model->date_update = null;

        $this->saveModel($model);

        $fileRelativePath = $model->getFileRelativePath();
        $filePath = $this->pathResolver->full($fileRelativePath);

        $this->logHandleManager->run($filePath, $model->id);

        $model->refresh();

        $model->is_completed = true;
        $model->is_run = false;
        $model->date_update = time();

        $this->saveModel($model);
    }

    public function deleteModel(LogImport $model): void
    {
        $result = $model->delete();

        if ($result === false) {
            throw new ShowUserException("Ошибка удаления модели id=$model->id");
        }
    }

    public function findModel(int $id): LogImport
    {
        $model = LogImport::find()
            ->byId($id)
            ->one();

        if (!$model) {
            throw new ShowUserException("Модель не найдена id=$id");
        }

        return $model;
    }

    private function saveModel(LogImport $model): int
    {
        if (!$model->save()) {
            throw (new ShowUserException("Ошибка сохранения модели id=$model->id"))->setDetails($model->getErrorSummary(true));
        }

        return $model->id;
    }

    private function extractDateFromFileName(string $fileName): string
    {
        preg_match('#\.(?<date>[\d-]+)\.#', $fileName, $m);
        return $m['date'] ?? '';
    }
}
