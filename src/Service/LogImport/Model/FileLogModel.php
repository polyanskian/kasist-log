<?php

namespace app\Service\LogImport\Model;

class FileLogModel extends \yii\base\Model
{
    public $files = [];
    public $isActionCreate = 0;

    public function rules()
    {
        return [
            [['isActionCreate'], 'boolean'],
            ['files', 'validateFilesRequired', 'skipOnEmpty' => false, 'skipOnError' => false],
        ];
    }

    public function validateFilesRequired(): void
    {
        if ($this->isActionCreate && !$this->files) {
            $this->addError('files', 'Необходимо выбрать файлы.');
        }
    }

    public function attributeLabels()
    {
        return [
            'files' => 'Файлы',
        ];
    }
}
