<?php

namespace app\Service\LogImport\Model;

class FindLogModel extends \yii\base\Model
{
    public $dateFrom = null;
    public $dateTo = null;
    public $dirs = [];

    public function rules()
    {
        $formatter = \Yii::$app->formatter;

        return [
            [['dateFrom', 'dirs'], 'required'],
            [['dateFrom', 'dateTo'], 'filter', 'filter' => function($value) use ($formatter) {
                return $formatter->asDbDate($value);
            }, 'skipOnEmpty' => true],
            [['dateFrom', 'dateTo'], 'date', 'format' => "php:$formatter->dbDateFormat"],
        ];
    }

    public function attributeLabels()
    {
        return [
            'dateFrom' => 'Дата начала',
            'dateTo' => 'Дата окончания',
            'dirs' => 'Директории',
        ];
    }
}
