<?php

declare(strict_types=1);

namespace app\Service\LogDir;

use app\AppFileHelper;
use app\AppParams;
use app\Exception\ShowUserException;
use app\models\LogDir;
use app\PathResolver;

class LogDirService
{
    private AppParams $appParams;
    private AppFileHelper $fileHelper;
    private PathResolver $pathResolver;

    public function __construct(AppParams $appParams, AppFileHelper $fileHelper, PathResolver $pathResolver)
    {
        $this->appParams = $appParams;
        $this->fileHelper = $fileHelper;
        $this->pathResolver = $pathResolver;
    }

    /**
     * @return LogDir[]
     */
    public function findAllDirs(): array
    {
        return LogDir::find()
            ->orderBy(['name' => SORT_ASC])
            ->all();
    }

    public function getLogRelativeDirPaths(): array
    {
        $logDir = $this->appParams->getDirRequestLog();

        $dirBasePath = $this->pathResolver->getBasePath();
        $logDirPath = $this->pathResolver->full($logDir);

        $logRelativeDirs = $this->fileHelper->findDirectories($logDirPath, $dirBasePath, true);
        sort($logRelativeDirs);
        return $logRelativeDirs;
    }

    public function findModel(int $id): LogDir
    {
        $model = LogDir::find()
            ->byId($id)
            ->one();

        if (!$model) {
            throw new ShowUserException("Модель не найдена id=$id");
        }

        return $model;
    }

    public function createModel(): LogDir
    {
        return new LogDir();
    }
}
