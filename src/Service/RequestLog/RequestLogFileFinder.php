<?php

declare(strict_types=1);

namespace app\Service\RequestLog;

use app\DateTimeHelper;
use app\DbTableName;
use app\models\LogImport;
use app\PathResolver;
use yii\db\ActiveQuery;

class RequestLogFileFinder
{
    private PathResolver $pathResolver;
    private DateTimeHelper $dateTimeHelper;

    public function __construct(
        PathResolver $pathResolver,
        DateTimeHelper $dateTimeHelper
    ) {
        $this->pathResolver = $pathResolver;
        $this->dateTimeHelper = $dateTimeHelper;
    }

    public function getLogFileNames(\DateTime $dateFrom, \DateTime $dateTo, array $dirs): array
    {
        $dates = $this->dateTimeHelper->getBetweenDates($dateFrom, $dateTo, 'Y-m-d');
        $fileNames = $this->getFileNamesByDates($dates);
        $fileRelativePaths = $this->getFileRelativePaths($fileNames, $dirs);

        $dirs = array_map('dirname', $fileRelativePaths);
        $dirs = array_unique($dirs);

        $fileNames = array_map('basename', $fileRelativePaths);
        $fileNames = array_unique($fileNames);

        $tableDir = DbTableName::LOG_DIRECTORY;

        $imports = LogImport::find()
            ->joinWith('logDirectory')
            ->andWhere(['file_name' => $fileNames])
            ->andWhere(["$tableDir.directory" => $dirs])
            ->all();

        $fileExistsDbRelativePaths = array_map(function (LogImport $model) {
            return "{$model->logDirectory->directory}/{$model->file_name}";
        }, $imports);

        $fileDiffRelativePaths = array_diff($fileRelativePaths, $fileExistsDbRelativePaths);

        return $this->getFileExistsRelativePaths($fileDiffRelativePaths);
    }

    private function getFileExistsRelativePaths(array $relativePaths): array
    {
        foreach ($relativePaths as $key => $relativePath) {
            $path = $this->pathResolver->full($relativePath);

            if (!is_file($path)) {
                unset($relativePaths[$key]);
            }
        }

        return $relativePaths;
    }

    private function getFileRelativePaths(array $fileNames, array $dirs): array
    {
        $result = [];

        foreach ($dirs as $dir) {
            foreach ($fileNames as $fileName) {
                $result[] = "$dir/$fileName";
            }
        }

        return $result;
    }

    private function getFileNamesByDates(array $dates): array
    {
        return array_map(function (string $date) {
            return $this->getFileNameByDate($date);
        }, $dates);
    }

    private function getFileNameByDate($date): string
    {
        $logFileDate = \Yii::$app->formatter->asDate($date, 'php:Y-m-d');
        return "access.$logFileDate.log";
    }
}
