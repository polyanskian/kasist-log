<?php

namespace app\Service\RequestLog\Import;

use app\Formatter;
use app\models\Ip;
use app\models\LogImport;
use app\models\RequestLog;
use app\PathResolver;
use app\Service\RequestLog\Parser\RequestLogFileParser;

class RequestLogImporter
{
    private RequestLogFileParser $parser;
    private PathResolver $pathResolver;
    private Formatter $formatter;

    public function __construct(
        PathResolver $pathResolver,
        RequestLogFileParser $parser,
        Formatter $formatter
    ) {
        $this->parser = $parser;
        $this->pathResolver = $pathResolver;
        $this->formatter = $formatter;
    }

    public function import(LogImport $modelImport)
    {
        $fileRelativePath = "{$modelImport->logDirectory->directory}/$modelImport->file_name";
        $filePath = $this->pathResolver->full($fileRelativePath);

        $data = $this->getFileData($filePath);

        $ips = array_column($data, 'ip');
        $ips = array_unique($ips);

        $ipModels = Ip::find()
            ->byIps($ips)
            ->indexBy('ip')
            ->all();

        $countData = count($data);

        foreach ($data as $key => $dataItem) {
//            echo "$key of $countData\n";

            $date = $dataItem['date'] ?? '';
            $ip = $dataItem['ip'] ?? '';
            $soft = $dataItem['soft'] ?? '';

            $ipModel = $ipModels[$ip] ?? null;

            $logModel = new RequestLog();
            $logModel->soft = $soft;
            $logModel->date = $this->formatter->asDbDateTime($date);

            $logModel->link('import', $modelImport);

            if ($ipModel) {
                $logModel->link('ip', $ipModel);
            } else {
                $logModel->ip_unknown = $ip;
            }

            if (!$logModel->save()) {
                $msgErrors = implode(', ', $logModel->getErrorSummary(true));
                throw new \RuntimeException("Ошибка сохранения модели [$msgErrors]");
            }
        }
    }

    private function getFileData(string $filePath)
    {
        $file = (new \SplFileObject($filePath));

        $file->setFlags(
            \SplFileObject::SKIP_EMPTY
            | \SplFileObject::READ_AHEAD
            | \SplFileObject::DROP_NEW_LINE
        );

        $result = [];

        foreach ($file as $line) {
            $line = $this->parser->prepareLine($line);

            if ($this->parser->isDetectDataLine($line)) {
                $result[] = $this->parser->parseLine($line);
            }
        }

        $file = null;
        return $result;
    }
}
