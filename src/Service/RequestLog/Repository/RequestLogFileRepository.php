<?php

namespace app\Service\RequestLog\Repository;

use app\Exception\ShowUserException;
use app\PathResolver;

class RequestLogFileRepository
{
//    private string $detectSubstr;
//    private string $pattern;
//    private PathResolver $pathResolver;
//
//    public function __construct(string $detectSubstr, string $pattern, PathResolver $pathResolver)
//    {
//        $this->pattern = $pattern;
//        $this->detectSubstr = $detectSubstr;
//        $this->pathResolver = $pathResolver;
//    }
//

//    public function getData(string $relativePath): array
//    {
//        $path = $this->pathResolver->full($relativePath);
//
//        if (!is_file($path)) {
//            throw new ShowUserException("Not exists file `$path`");
//        }
//
//        if (!is_readable($path)) {
//            throw new ShowUserException("Not readable file `$path`");
//        }
//
//        return $this->readData($path);
//    }
//
//    private function readData(string $path): array
//    {
//        $result = [];
//
//        $file = fopen($path, 'rb');
//
//        while(!feof($file)) {
//            $line = fgets($file);
//            $line = $this->prepareLine($line);
//
//            if ($this->isDetectDataLine($line)) {
//                $result[] = $this->parseData($line);
//            }
//        }
//
//        fclose($file);
//
//        return $result;
//    }
//
//    private function prepareLine(string $line): string
//    {
//        $line = urldecode($line);
//        return trim($line);
//    }
//
//    private function parseData(string $line): array
//    {
//        if (preg_match($this->pattern, $line, $m)) {
//            $date = $m[1] ?? '';
//            $ip = $m[2] ?? '';
//            $soft = $m[3] ?? '';
//
//            $date = trim($date);
//
//            $ip = trim($ip);
//            $soft = trim($soft);
//
//            return [
//                'date' => $date,
//                'ip' => $ip,
//                'soft' => $soft,
//            ];
//        }
//
//        return [];
//    }
//
//    private function isDetectDataLine(string $line): bool
//    {
//        return strpos($line, $this->detectSubstr);
//    }

//    public function getAllowLogFileNames(string $from, string $to, array $dirs): array
//    {
//        $dateFrom = new \DateTime($from);
//        $dateTo = new \DateTime($to);
//
//        $dates = $this->getDateBetween($dateFrom, $dateTo);
//
//        $fileNames = $this->getFileNamesByDates($dates);
//        $fileRelativePaths = $this->getFileRelativePaths($fileNames, $dirs);
//
//        $fileExistsDbRelativePaths = RequestImport::find()
//            ->select('file_name')
//            ->asArray()
//            ->column();
//
//        $fileDiffRelativePaths = array_diff($fileRelativePaths, $fileExistsDbRelativePaths);
//
//        return $this->getFileExistsRelativePaths($fileDiffRelativePaths);
//    }
//
//    private function getFileExistsRelativePaths(array $relativePaths): array
//    {
//        foreach ($relativePaths as $key => $relativePath) {
//            $path = $this->pathResolver->full($relativePath);
//
//            if (!is_file($path)) {
//                unset($relativePaths[$key]);
//            }
//        }
//
//        return $relativePaths;
//    }
//
//    private function getFileRelativePaths(array $fileNames, array $dirs): array
//    {
//        $result = [];
//
//        foreach ($dirs as $dir) {
//            foreach ($fileNames as $fileName) {
//                $result[] = "$dir/$fileName";
//            }
//        }
//
//        return $result;
//    }
//
//    private function getFileNamesByDates(array $dates): array
//    {
//        return array_map(function (string $date) {
//            return $this->getFileNameByDate($date);
//        }, $dates);
//    }
//
//    private function getDateBetween(\DateTime $dateFrom, \DateTime $dateTo): array
//    {
//        return $this->dateTimeHelper->getBetweenDates($dateFrom, $dateTo, 'Y-m-d');
//    }
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//    public function getData(string $relativePath): array
//    {
//        $path = $this->pathResolver->full($relativePath);
//
//        if (!is_file($path)) {
//            throw new ShowUserException("Not exists file `$path`");
//        }
//
//        if (!is_readable($path)) {
//            throw new ShowUserException("Not readable file `$path`");
//        }
//
//        return $this->readData($path);
//    }
}
