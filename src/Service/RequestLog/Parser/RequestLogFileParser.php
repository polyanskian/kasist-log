<?php

namespace app\Service\RequestLog\Parser;

class RequestLogFileParser
{
    private string $detectSubstr;
    private string $pattern;

    public function __construct(string $detectSubstr, string $pattern) {
        $this->pattern = $pattern;
        $this->detectSubstr = $detectSubstr;
    }

    public function parseLine(string $line): array
    {
        if (preg_match($this->pattern, $line, $m)) {
            $date = $m[1] ?? '';
            $ip = $m[2] ?? '';
            $soft = $m[3] ?? '';

            $date = trim($date);

            $ip = trim($ip);
            $soft = trim($soft);

            return [
                'date' => $date,
                'ip' => $ip,
                'soft' => $soft,
            ];
        }

        return [];
    }

    public function prepareLine(string $line): string
    {
        $line = urldecode($line);
        return trim($line);
    }

    public function isDetectDataLine(string $line): bool
    {
        return strpos($line, $this->detectSubstr);
    }
}
