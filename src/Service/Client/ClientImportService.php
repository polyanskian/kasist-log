<?php

declare(strict_types=1);

namespace app\Service\Client;

use app\Exception\ShowUserException;
use app\PathResolver;
use app\Service\Client\Import\ClientImporter;
use app\Service\Client\Import\CompanyImporter;
use app\Service\Client\Repository\ImportClientRepositoryFactory;

class ClientImportService
{
    private ClientImporter $clientImporter;
    private PathResolver $pathResolver;
    private ImportClientRepositoryFactory $importClientRepositoryFactory;
    private CompanyImporter $companyImporter;

    public function __construct(
        PathResolver $pathResolver,
        ImportClientRepositoryFactory $importClientRepositoryFactory,
        ClientImporter $clientImporter,
        CompanyImporter $companyImporter
    ) {
        $this->clientImporter = $clientImporter;
        $this->pathResolver = $pathResolver;
        $this->importClientRepositoryFactory = $importClientRepositoryFactory;
        $this->companyImporter = $companyImporter;
    }

    public function importCompanies(string $fileRelativePath): void
    {
        $this->companyImporter->import($fileRelativePath);
    }

    public function importClients(array $dbRelativePaths): void
    {
        foreach ($dbRelativePaths as $dbRelativePath) {
            $dbPath = $this->pathResolver->full($dbRelativePath);

            if (!is_file($dbPath)) {
                throw new ShowUserException("Файл не найден `$dbPath`");
            }

            $clientRepository = $this->importClientRepositoryFactory->createRepository($dbPath);
            $this->clientImporter->import($clientRepository);
        }
    }
}
