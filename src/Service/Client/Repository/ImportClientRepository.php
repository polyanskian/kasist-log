<?php

namespace app\Service\Client\Repository;

use yii\db\BatchQueryResult;
use yii\db\Connection;
use yii\db\Expression;
use yii\db\Query;

class ImportClientRepository
{
    private Connection $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function getClientDataAll($batchSize = 100): BatchQueryResult
    {
        /*
        SELECT DISTINCT
            a.ip,
            a.login
        FROM ActionLog a
        WHERE
            a.login NOT NULL
            AND a.ip NOT NULL
        ORDER BY a.login ASC
        */

        $query = (new Query())
            ->distinct()
            ->from('ActionLog')
            ->select(['login', 'ip'])
            ->andWhere(new Expression('login NOT NULL AND ip NOT NULL'))
            ->orderBy(['login' => SORT_ASC])
        ;

        return $query->batch($batchSize, $this->connection);
//        return $query->each($batchSize, $this->connection);
    }
}
