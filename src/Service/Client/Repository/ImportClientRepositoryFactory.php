<?php

namespace app\Service\Client\Repository;

use app\Db\SqliteConnectionFactory;

class ImportClientRepositoryFactory
{
    private SqliteConnectionFactory $connectionFactory;

    public function __construct(SqliteConnectionFactory $connectionFactory)
    {
        $this->connectionFactory = $connectionFactory;
    }

    public function createRepository(string $pathFileDb): ImportClientRepository
    {
        $connection = $this->connectionFactory->createConnection($pathFileDb);
        return new ImportClientRepository($connection);
    }
}
