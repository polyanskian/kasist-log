<?php

namespace app\Service\Client\Import;

use app\models\Client;
use app\models\Ip;
use app\Service\Client\Repository\ImportClientRepository;

class ClientImporter
{
    public function import(ImportClientRepository $repository): void
    {
        $clientDatas = $repository->getClientDataAll();

        foreach ($clientDatas as $clientData) {
            $logins = array_column($clientData, 'login');
            $logins = array_unique($logins);
            $this->importLogins($logins);

            $ips = array_column($clientData, 'ip');
            $ips = array_unique($ips);
            $this->importIps($ips);

            $this->linkClientToIp($clientData, $logins, $ips);
        }
    }

    private function linkClientToIp(array $clientData, array $logins, array $ips): void
    {
        $clientModels = Client::find()
            ->with('ips')
            ->andWhere(['login' => $logins])
            ->indexBy('login')
            ->all();

        $ipModels = Ip::find()
            ->andWhere(['ip' => $ips])
            ->indexBy('ip')
            ->all();

        $rows = [];

        foreach ($clientData as $clientDataItem) {
            $login = $clientDataItem['login'] ?? '';
            $ip = $clientDataItem['ip'] ?? '';

            $clientModel = $clientModels[$login] ?? null;
            $clientIps = array_column($clientModel->ips, 'ip');

            if (!in_array($ip, $clientIps, true)) {
                $ipModel = $ipModels[$ip] ?? null;

                // insert optimization
                $keyUnique = "{$clientModel->id}|{$ipModel->id}";
                $rows[$keyUnique] = [$clientModel->id, $ipModel->id];
            }
        }

        \Yii::$app->db->createCommand()
            ->batchInsert('client_to_ip', ['client_id', 'ip_id'], $rows)
            ->execute();
    }

    private function importLogins(array $logins): void
    {
        $clientLogins = Client::find()
            ->select('login')
            ->andWhere(['login' => $logins])
            ->column();

        $diffLogins = array_diff($logins, $clientLogins);

        if ($diffLogins) {
            $diffLogins = array_map(function (string $login) {
                return [$login];
            }, $diffLogins);

            Client::getDb()->createCommand()
                ->batchInsert(Client::tableName(), ['login'], $diffLogins)
                ->execute();
        }
    }

    private function importIps(array $ips): void
    {
        $clientIps = Ip::find()
            ->select('ip')
            ->andWhere(['ip' => $ips])
            ->column();

        $diffIps = array_diff($ips, $clientIps);

        if ($diffIps) {
            $diffIps = array_map(function (string $ip) {
                return [$ip];
            }, $diffIps);

            Ip::getDb()->createCommand()
                ->batchInsert(Ip::tableName(), ['ip'], $diffIps)
                ->execute();
        }
    }
}



