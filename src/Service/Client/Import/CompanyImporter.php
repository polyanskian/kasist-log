<?php

namespace app\Service\Client\Import;

use app\Exception\ShowUserException;
use app\models\Client;
use app\PathResolver;
use yii\helpers\BaseStringHelper;
use yii\helpers\Html;

class CompanyImporter
{
    private PathResolver $pathResolver;

    public function __construct(PathResolver $pathResolver)
    {
        $this->pathResolver = $pathResolver;
    }

    public function import(string $fileRelativePath): void
    {
        $filePath = $this->pathResolver->full($fileRelativePath);

        if (!is_file($filePath)) {
            throw new \InvalidArgumentException("Файл не найден `$fileRelativePath`");
        }

        $file = new \SplFileObject($filePath);

        $file->setFlags(\SplFileObject::READ_CSV
            | \SplFileObject::SKIP_EMPTY
            | \SplFileObject::READ_AHEAD
            | \SplFileObject::DROP_NEW_LINE
        );

        $file->seek(PHP_INT_MAX);
        $countTotal = $file->key() + 1;
        $file->rewind();

        foreach ($this->readBatch($file, $fileRelativePath, 1000) as $companyDatas) {
            $clientIds = array_column($companyDatas, 'id');

            $clients = Client::find()
                ->byLogins($clientIds)
                ->byEmptyName()
                ->indexBy('login')
                ->all();

//            echo $file->key() . " of $countTotal | clients: " . count($clients) . PHP_EOL;

            if (!$clients) {
                continue;
            }

            foreach ($clients as $client) {
                $company = $companyDatas[$client->login] ?? [];

                if (!$company) {
                    throw new \RuntimeException("Компания не найдена `login=$client->login`");
                }

                $client->name = $company['name'];
                $client->save(false);

                $errors = $client->getErrorSummary(true);

                if ($errors) {
                    $msg = "Ошибка валидации `clientId=$client->id<hr>"
                        . Html::ol($errors);

                    throw new ShowUserException($msg);
                }
            }
        }
    }

    private function readBatch($file, string $fileRelativePath, $size = 100): \Generator
    {
        $counter = 0;
        $buffer = [];

        while (!$file->eof()) {
            $row = $file->fgetcsv();

            if (!$row) {
                continue;
            }

            if ($counter > $size) {
                $buffer = [];
                $counter = 0;
            }

            try {
                $row = $this->parseCompanyData($row);
            } catch (\RuntimeException $e) {
                $msg = $this->getMessageDetailError($e->getMessage(), $fileRelativePath, $file->key(), $file->getCurrentLine());
                throw new ShowUserException($msg);
            }

            $id = $row['id'];

            if (array_key_exists($id, $buffer)) {
                $msg = $this->getMessageDetailError("ID компани дублируется `$id`", $fileRelativePath, $file->key(), $file->getCurrentLine());
                throw new ShowUserException($msg);
            }

            $buffer[$id] = $row;
            $counter++;

            if ($counter === $size) {
                $counter++;
                yield $buffer;
            }
        }

        if ($counter < $size) {
            yield $buffer;
        }
    }

    private function parseCompanyData(array $row): array
    {
        $id = $row[0] ?? '';
        $id = trim($id);

        if ($id === '') {
            throw new \RuntimeException('ID организации не найден');
        }

        $name = $row[1] ?? '';
        $name = str_replace('  ', ' ', $name);
        $name = trim($name);
        $name = mb_strtolower($name);
        $name = BaseStringHelper::mb_ucfirst($name);

        if ($name === '') {
            throw new \RuntimeException('Имя организации не найдено');
        }

        return [
            'id' => $id,
            'name' => $name,
        ];
    }

    private function getMessageDetailError(string $msg, string $fileRelativePath, int $lineNumber, string $line): string
    {
        return "$msg<hr>Ошибка в файле `$fileRelativePath`<br>Строка №: $lineNumber<br>`$line`";
    }
}
