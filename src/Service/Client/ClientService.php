<?php

declare(strict_types=1);

namespace app\Service\Client;

use app\jobs\ImportClientJob;
use app\models\Client;
use yii\queue\Queue;

class ClientService
{
    private Queue $queue;

    public function __construct(Queue $queue) {
        $this->queue = $queue;
    }

    public function startImport(): void
    {
        $this->queue->push(new ImportClientJob());
    }

    public function deleteAllClients(): void
    {
        Client::deleteAll();
    }
}
