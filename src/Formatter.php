<?php

namespace app;

class Formatter extends \yii\i18n\Formatter
{
    public $dbDateTimeFormat = 'Y-m-d H:i:s';
    public $dbDateFormat = 'Y-m-d';

    public function asDbDateTime($date): string
    {
        return $this->asDatetime($date, "php:$this->dbDateTimeFormat");
    }

    public function asDbDate($date): string
    {
        return $this->asDate($date, "php:$this->dbDateFormat");
    }
}
