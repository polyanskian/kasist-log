<?php

namespace app;

class DateTimeHelper
{
    public function getBetweenDates(\DateTime $dateFrom, \DateTime $dateTo, string $resultFormat = 'Y-m-d'): array
    {
        $dateTo = clone $dateTo;
        $dateTo->modify('+1 day');

        if ($dateFrom > $dateTo) {
            $fromAsString = $dateFrom->format('Y.m.d');
            $toAsString = $dateTo->format('Y.m.d');
            throw new \InvalidArgumentException("Дата начала меньше даты окончания. `$fromAsString > $toAsString`");
        }

        $between = new \DatePeriod($dateFrom, new \DateInterval('P1D'), $dateTo);

        return array_map(function($date) use ($resultFormat) {
            return $date->format($resultFormat);
        }, iterator_to_array($between));
    }
}
