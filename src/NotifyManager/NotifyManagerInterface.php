<?php

namespace app\NotifyManager;

interface NotifyManagerInterface
{
    public function success(string $message, array $details = [], $isAdd = false, $removeAfterAccess = true): self;
    public function error(string $message, array $details = [], $isAdd = false, $removeAfterAccess = true): self;
    public function warning(string $message, array $details = [], $isAdd = false, $removeAfterAccess = true): self;
    public function info(string $message, array $details = [], $isAdd = false, $removeAfterAccess = true): self;
    public function custom(string $message, array $details = [], string $key = '', $isAdd = false, $removeAfterAccess = true): self;
}
