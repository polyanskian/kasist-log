<?php

namespace app\NotifyManager;

use yii\helpers\Html;
use yii\web\Session;

class NotifyManager implements NotifyManagerInterface
{
    public const SUCCESS = 'success';
    public const ERROR = 'error';
    public const WARNING = 'warning';
    public const INFO = 'info';

    private Session $session;

    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    public function success(string $message, array $details = [], $isAdd = false, $removeAfterAccess = true): self
    {
        return $this->flashMessage(self::SUCCESS, $message, $details, $isAdd, $removeAfterAccess);
    }

    public function error(string $message, array $details = [], $isAdd = false, $removeAfterAccess = true): self
    {
        return $this->flashMessage(self::ERROR, $message, $details, $isAdd, $removeAfterAccess);
    }

    public function warning(string $message, array $details = [], $isAdd = false, $removeAfterAccess = true): self
    {
        return $this->flashMessage(self::WARNING, $message, $details, $isAdd, $removeAfterAccess);
    }

    public function info(string $message, array $details = [], $isAdd = false, $removeAfterAccess = true): self
    {
        return $this->flashMessage(self::INFO, $message, $details, $isAdd, $removeAfterAccess);
    }

    public function custom(string $message, array $details = [], string $key = '', $isAdd = false, $removeAfterAccess = true): self
    {
        return $this->flashMessage($key, $message, $details, $isAdd, $removeAfterAccess);
    }

    private function flashMessage(string $key, string $message, array $details, $isAdd = false, $removeAfterAccess = true): self
    {
        if ($details) {
            $message .= '<hr>' . Html::ol($details);
        }

        if ($isAdd) {
            $this->session->addFlash($key, $message, $removeAfterAccess);
            return $this;
        }

        $this->session->setFlash($key, $message, $removeAfterAccess);
        return $this;
    }
}
