<?php

namespace app;

use yii\helpers\FileHelper;

class AppFileHelper
{
    public function findDirectories(string $pathDir, string $dirBase, bool $isRelative = false): array
    {
        if ($pathDir === '') {
            throw new \InvalidArgumentException('Не задан параметр `dirBase`');
        }
        
        if (!is_dir($pathDir)) {
            throw new \InvalidArgumentException("Директория не найдена `$pathDir`");
        }

        $dirPaths = FileHelper::findDirectories($pathDir); // glob("$pathDir/*", GLOB_ONLYDIR);

        if ($isRelative) {
            foreach ($dirPaths as $key => $dirPath) {
                $dirPath = str_replace("^$dirBase", '', "^$dirPath");
                $dirPath = ltrim($dirPath, '^/');
                $dirPaths[$key] = $dirPath;
            }
        }

        return $dirPaths;
    }
}
