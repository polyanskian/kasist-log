<?php

namespace app;

class AppParams
{
    private $dirRequestLog = '';
    private $dbClientPaths = [];
    private $dataCompanyPath = '';

    public function __construct(array $params)
    {
        $this->setFromArray($params);
    }

    public function getDataCompanyPath(): string
    {
        return $this->dataCompanyPath;
    }

    public function getDbClientPaths(): array
    {
        return $this->dbClientPaths;
    }

    public function getDirRequestLog(): string
    {
        return $this->dirRequestLog;
    }

    private function setFromArray(array $params): void
    {
        foreach ($params as $property => $value) {
            if (!property_exists($this, $property)) {
                throw new \InvalidArgumentException("Свойство класса не существует `$property`");
            }

            $this->$property = $value;
        }
    }
}
