<?php

namespace app\Exception;

class ShowUserException extends \Exception
{
    private array $details = [];

    public function getDetails(): array
    {
        return $this->details;
    }

    public function setDetails(array $details): self
    {
        $this->details = $details;
        return $this;
    }
}
