<?php

namespace app;

final class DbTableName
{
    public const CLIENT = 'client';
    public const CLIENT_TO_IP = 'client_to_ip';
    public const IP = 'ip';
    public const LOG_DIRECTORY = 'log_dir';
    public const LOG_IMPORT = 'log_import';

    public const REQUEST_SERVER_INFO = 'request_server_info';
    public const REQUEST_SEARCH_TEXT = 'request_search_text';
    public const REQUEST_DOC_INFO_ND = 'request_doc_info_nd';
    public const REQUEST_MAINPAGE_SOURCE = 'request_mainpage';
    public const REQUEST_DND = 'request_dnd';
    public const REQUEST_PERSONALDATA = 'request_personaldata';
    public const REQUEST_SEARCHQ = 'request_searchq';
}
