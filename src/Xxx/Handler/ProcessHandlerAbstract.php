<?php

namespace app\Xxx\Handler;

use app\models\Ip;

abstract class ProcessHandlerAbstract implements ProcessHandlerInterface
{
    abstract protected function getDetectPattern(): string;
    abstract protected function getParsePattern(): string;

    abstract public function getIsSkip(int $importId): bool;
    abstract protected function saveBatchInsert($cols, $rows): void;

    public function validateParseData(array $data): array
    {
        $errors = [];

        foreach ($data as $key => $val) {
            if ($val === '') {
                $errors[] = "Пустое значение для `$key`";
            }
        }

        return $errors;
    }

    public function isDetect(string $line): bool
    {
        return strpos($line, $this->getDetectPattern()) !== false;
    }

    public function saveDatas(array $datas, int $importId, $batchSize = 100): void
    {
        $ips = array_column($datas, 'ip');
        $ipModels = $this->getIpModels($ips);

        $chunks = array_chunk($datas, $batchSize);

        foreach ($chunks as $chunk) {
            $rows = [];

            foreach ($chunk as $data) {
                $ip = $data['ip'] ?? '';
                $ipModel = $ipModels[$ip] ?? null;

                $rows[] = $this->getRow($data, $importId, $ipModel);
            }

            $cols = $this->getCols();

            $this->saveBatchInsert($cols, $rows);
        }
    }

    public function parse(string $line): array
    {
        preg_match($this->getParsePattern(), $line, $m);

        $result = [
            'ip' => $m['ip'] ?? '',
            'soft' => (isset($m['soft'])) ? mb_strtolower($m['soft']) : '',
            'date' => $m['date'] ?? '',
        ];

        $result = array_map('trim', $result);

        return $result;
    }

    protected function getCols(): array
    {
        return [
            'import_id',
            'ip_id',
            'ip_unknown',
            'soft',
            'date',
        ];
    }

    protected function getRow(array $data, int $importId, ?Ip $ipModel): array
    {
        return [
            $importId,
            ($ipModel) ? $ipModel->id : null,
            ($ipModel) ? null : $data['ip'],
            $data['soft'],
            $data['date'],
        ];
    }

    /**
     * @return Ip[]
     */
    protected function getIpModels(array $ips): array
    {
        $ips = array_unique($ips);

        return Ip::find()
            ->byIps($ips)
            ->indexBy('ip')
            ->all();
    }
}
