<?php

namespace app\Xxx\Handler;

use app\models\MainpageSource;

class MainpageSourceHandler extends ProcessHandlerAbstract
{
    private string $detect;
    private string $pattern;

    public function __construct(
        string $detect = 'GET /mainpage?source=kassist',
        string $pattern = '#^\[(?<date>[\d\- :]+)\]: (:.+:)?(?<ip>[\d.]+) -- GET \/mainpage\?source=kassist.+\/x\d\d\/(?<soft>.+)\/#si'
    ) {
        $this->detect = $detect;
        $this->pattern = $pattern;
    }

    protected function saveBatchInsert($cols, $rows): void
    {
        MainpageSource::getDb()
            ->createCommand()
            ->batchInsert(MainpageSource::tableName(), $cols, $rows)
            ->execute();
    }

    public function getIsSkip(int $importId): bool
    {
        return MainpageSource::find()
            ->byImportId($importId)
            ->exists();
    }

    protected function getParsePattern(): string
    {
        return $this->pattern;
    }

    protected function getDetectPattern(): string
    {
        return $this->detect;
    }
}
