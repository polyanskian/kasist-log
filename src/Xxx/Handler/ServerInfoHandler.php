<?php

namespace app\Xxx\Handler;

use app\models\ServerInfo;

class ServerInfoHandler extends ProcessHandlerAbstract
{
    private string $detect;
    private string $pattern;

    public function __construct(
        string $detect = 'GET /.apiServerInfo?kassist',
//        string $pattern = '#^\[(?<date>[\d\- :]+)\]: (?<ip>[\d.]+) -- GET \/\.apiServerInfo\?kassist\/[\d./]+\/[\dx]+\/(?<soft>.+)\/#si'
        string $pattern = '#^\[(?<date>[\d\- :]+)\]: (:.+:)?(?<ip>[\d.]+) -- GET \/\.apiServerInfo\?kassist\/[\d./]+\/[\dx]+\/(?<soft>.+)\/#si'
    ) {
        $this->detect = $detect;
        $this->pattern = $pattern;
    }

    protected function saveBatchInsert($cols, $rows): void
    {
        ServerInfo::getDb()
            ->createCommand()
            ->batchInsert(ServerInfo::tableName(), $cols, $rows)
            ->execute();
    }

    public function getIsSkip(int $importId): bool
    {
        return ServerInfo::find()
            ->byImportId($importId)
            ->exists();
    }

    protected function getParsePattern(): string
    {
        return $this->pattern;
    }

    protected function getDetectPattern(): string
    {
        return $this->detect;
    }
}
