<?php

namespace app\Xxx\Handler;

use app\models\DocInfoNd;

class DocInfoNdHandler extends ProcessHandlerAbstract
{
    private string $detect;
    private string $pattern;

    public function __construct(
        string $detect = 'GET /.apiDocInfo?nd',
        string $pattern = '#^\[(?<date>[\d\- :]+)\]: (:.+:)?(?<ip>[\d.]+) -- GET \/\.apiDocInfo\?nd.+\/x\d\d\/(?<soft>.+)\/#si'
    ) {
        $this->detect = $detect;
        $this->pattern = $pattern;
    }

    protected function saveBatchInsert($cols, $rows): void
    {
        DocInfoNd::getDb()
            ->createCommand()
            ->batchInsert(DocInfoNd::tableName(), $cols, $rows)
            ->execute();
    }

    public function getIsSkip(int $importId): bool
    {
        return DocInfoNd::find()
            ->byImportId($importId)
            ->exists();
    }

    protected function getParsePattern(): string
    {
        return $this->pattern;
    }

    protected function getDetectPattern(): string
    {
        return $this->detect;
    }
}
