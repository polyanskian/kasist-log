<?php

namespace app\Xxx\Handler;

use app\models\Ip;
use app\models\Personaldata;

class PersonaldataHandler extends ProcessHandlerAbstract
{
    private string $detect;
    private string $pattern;

    public function __construct(
        string $detect = 'POST /docs/api/personaldata?key',
//        string $pattern = '#^\[(?<date>[\d\- :]+)\]: (?<ip>[\d.]+) -- POST \/docs\/api\/personaldata\?key#si'
        string $pattern = '#^\[(?<date>[\d\- :]+)\]: (:.+:)?(?<ip>[\d.]+) -- POST \/docs\/api\/personaldata\?key#si'
    ) {
        $this->detect = $detect;
        $this->pattern = $pattern;
    }

    public function parse(string $line): array
    {
        preg_match($this->getParsePattern(), $line, $m);

        $result = [
            'ip' => $m['ip'] ?? '',
//            'soft' => (isset($m['soft'])) ? mb_strtolower($m['soft']) : '',
//            'date' => $m['date'] ?? '',
        ];

        $result = array_map('trim', $result);

        return $result;
    }

    protected function getCols(): array
    {
        return [
            'import_id',
            'ip_id',
            'ip_unknown',
//            'soft',
//            'date',
        ];
    }

    protected function getRow(array $data, int $importId, ?Ip $ipModel): array
    {
        return [
            $importId,
            ($ipModel) ? $ipModel->id : null,
            ($ipModel) ? null : $data['ip'],
//            $data['soft'],
//            $data['date'],
        ];
    }

    protected function saveBatchInsert($cols, $rows): void
    {
        Personaldata::getDb()
            ->createCommand()
            ->batchInsert(Personaldata::tableName(), $cols, $rows)
            ->execute();
    }

    public function getIsSkip(int $importId): bool
    {
        return Personaldata::find()
            ->byImportId($importId)
            ->exists();
    }

    protected function getParsePattern(): string
    {
        return $this->pattern;
    }

    protected function getDetectPattern(): string
    {
        return $this->detect;
    }
}
