<?php

namespace app\Xxx\Handler;

use app\models\SearchText;

class SearchTextHandler extends ProcessHandlerAbstract
{
    private string $detect;
    private string $pattern;

    public function __construct(
        string $detect = 'GET /.apiSearch?text',
//        string $pattern = '#^\[(?<date>[\d\- :]+)\]: (?<ip>[\d.]+) -- GET \/\.apiSearch\?text.+\/x\d\d\/(?<soft>.+)\/#si'
        string $pattern = '#^\[(?<date>[\d\- :]+)\]: (:.+:)?(?<ip>[\d.]+) -- GET \/\.apiSearch\?text=(.+[x864]\/(?<soft>.+)\/)?.+#si'
    ) {
        $this->detect = $detect;
        $this->pattern = $pattern;
    }

    public function validateParseData(array $data): array
    {
        if (isset($data['soft'])) {
            unset($data['soft']);
        }

        return parent::validateParseData($data);
    }

    protected function saveBatchInsert($cols, $rows): void
    {
        SearchText::getDb()
            ->createCommand()
            ->batchInsert(SearchText::tableName(), $cols, $rows)
            ->execute();
    }

    public function getIsSkip(int $importId): bool
    {
        return SearchText::find()
            ->byImportId($importId)
            ->exists();
    }

    protected function getParsePattern(): string
    {
        return $this->pattern;
    }

    protected function getDetectPattern(): string
    {
        return $this->detect;
    }
}
