<?php

namespace app\Xxx\Handler;

use app\models\Searchq;

class SearchqHandler extends ProcessHandlerAbstract
{
    private string $detect;
    private string $pattern;

    public function __construct(
        string $detect = 'GET /search?q',
//        string $pattern = '#^\[(?<date>[\d\- :]+)\]: (?<ip>[\d.]+) -- GET \/search\?q=.*source=kassist.+\/x\d\d\/(?<soft>.+)\/#si'
        string $pattern = '#^\[(?<date>[\d\- :]+)\]: (:.+:)?(?<ip>[\d.]+) -- GET \/search\?q=.*source=kassist.+\/x\d\d\/(?<soft>.+)\/#si'
    ) {
        $this->detect = $detect;
        $this->pattern = $pattern;
    }

    protected function saveBatchInsert($cols, $rows): void
    {
        Searchq::getDb()
            ->createCommand()
            ->batchInsert(Searchq::tableName(), $cols, $rows)
            ->execute();
    }

    public function getIsSkip(int $importId): bool
    {
        return Searchq::find()
            ->byImportId($importId)
            ->exists();
    }

    protected function getParsePattern(): string
    {
        return $this->pattern;
    }

    protected function getDetectPattern(): string
    {
        return $this->detect;
    }
}
