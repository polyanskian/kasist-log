<?php

namespace app\Xxx\Handler;

use app\models\Dnd;

class DndHandler extends ProcessHandlerAbstract
{
    private string $detect;
    private string $pattern;

    public function __construct(
        string $detect = 'GET /d?nd',
        string $pattern = '#^\[(?<date>[\d\- :]+)\]: (:.+:)?(?<ip>[\d.]+) -- GET \/d\?nd.+\/x\d\d\/(?<soft>.+)\/#si'
    ) {
        $this->detect = $detect;
        $this->pattern = $pattern;
    }

    protected function saveBatchInsert($cols, $rows): void
    {
        Dnd::getDb()
            ->createCommand()
            ->batchInsert(Dnd::tableName(), $cols, $rows)
            ->execute();
    }

    public function getIsSkip(int $importId): bool
    {
        return Dnd::find()
            ->byImportId($importId)
            ->exists();
    }

    protected function getParsePattern(): string
    {
        return $this->pattern;
    }

    protected function getDetectPattern(): string
    {
        return $this->detect;
    }
}
