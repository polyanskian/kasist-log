<?php

namespace app\Xxx;

interface LogFileInterface
{
    public function getCountLines(): int;
    public function getLineNumber(): int;
    public function isEof(): bool;
    public function readLine(): string;
    public function open(bool $validation = true): void;
    public function close(): void;
    public function validate(): string;
}
