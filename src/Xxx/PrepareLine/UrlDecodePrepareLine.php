<?php

namespace app\Xxx\PrepareLine;

class UrlDecodePrepareLine implements PrepareLineInterface
{
    public function prepare(string $line): string
    {
        return urldecode($line);
    }
}
