<?php

namespace app\Xxx\PrepareLine;

interface PrepareLineInterface
{
    public function prepare(string $line): string;
}
