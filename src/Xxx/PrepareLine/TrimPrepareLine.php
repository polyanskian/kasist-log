<?php

namespace app\Xxx\PrepareLine;

class TrimPrepareLine implements PrepareLineInterface
{
    public function prepare(string $line): string
    {
        return trim($line);
    }
}
