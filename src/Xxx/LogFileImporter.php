<?php

namespace app\Xxx;

use app\File\LogFile;
use app\Xxx\PrepareLine\PrepareLineInterface;
use app\Xxx\Processor\ProcessorInterface;

class LogFileImporter
{
    /**
     * @var ProcessorInterface[];
     */
    private $processors = [];

    /**
     * @var PrepareLineInterface[]
     */
    private $prepareLineHandlers = [];

    public function __construct(array $processors, array $prepareLineHandlers = [])
    {
        foreach ($processors as $processor) {
            $this->addProcessor($processor);
        }

        foreach ($prepareLineHandlers as $beforeLineHandler) {
            $this->addPrepareLineHandler($beforeLineHandler);
        }
    }

    public function run(string $filePath, int $importId)
    {
        $file = new LogFile($filePath);
        $file->open();
        $countLines = $file->getCountLines();

        $this->start($countLines, $filePath, $importId);

        while (!$file->isEof()) {
            $line = $file->readLine();
            $lineNum = $file->getLineNumber();

            $line = $this->prepareLine($line);
            $this->process($line, $lineNum, $countLines, $filePath, $importId);
        }

        $this->end($countLines, $filePath, $importId);

        $file->close();
    }

    private function start(int $countTotalLines, string $filePath, int $importId): void
    {
        foreach ($this->processors as $processor) {
            $processor->processStart($countTotalLines, $filePath, $importId);
        }
    }

    private function prepareLine(string $line): string
    {
        foreach ($this->prepareLineHandlers as $beforeLineHandler) {
            $line = $beforeLineHandler->prepare($line);
        }

        return $line;
    }

    private function process(string $line, int $numLine, int $countTotalLines, string $filePath, int $importId): void
    {
        foreach ($this->processors as $processor) {
            $processor->process($line, $numLine, $countTotalLines, $filePath, $importId);
        }
    }

    private function end(int $countTotalLines, string $filePath, int $importId): void
    {
        foreach ($this->processors as $processor) {
            $processor->processEnd($countTotalLines, $filePath, $importId);
        }
    }

    private function addPrepareLineHandler(PrepareLineInterface $handler): void
    {
        $key = get_class($handler);

        if (array_key_exists($key, $this->prepareLineHandlers)) {
            throw new \Exception("Обработчик уже существует key=$key");
        }

        $this->prepareLineHandlers[$key] = $handler;
    }
    
    private function addProcessor(ProcessorInterface $processor): void
    {
        $key = get_class($processor);
        
        if (array_key_exists($key, $this->processors)) {
            throw new \Exception("Обработчик уже существует key=$key");
        }
        
        $this->processors[$key] = $processor;
    }
}
