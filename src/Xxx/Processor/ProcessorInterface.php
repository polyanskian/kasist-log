<?php

namespace app\Xxx\Processor;

interface ProcessorInterface
{
    public function processStart(int $countTotalLines, string $filePath, int $importId): void;
    public function process(string $line, int $numLine, int $countTotalLines, string $filePath, int $importId): void;
    public function processEnd(int $countTotalLines, string $filePath, int $importId): void;
}
