<?php

namespace app\Xxx\Processor;

use app\Xxx\Handler\ProcessHandlerInterface;

abstract class DateIpSoftProcessorAbstract implements ProcessorInterface
{
    private ProcessHandlerInterface $handler;
    private bool $isSkip = false;
    private array $resultDatas;

    public function __construct(ProcessHandlerInterface $handler) {
        $this->handler = $handler;
    }

    public function processStart(int $countTotalLines, string $filePath, int $importId): void
    {
        $this->isSkip = $this->handler->getIsSkip($importId);
        $this->resultDatas = [];
    }

    public function process(string $line, int $numLine, int $countTotalLines, string $filePath, int $importId): void
    {
        if (!$this->isSkip && $this->handler->isDetect($line)) {
            $data = $this->handler->parse($line);
            $errors = $this->handler->validateParseData($data);

            if ($errors) {
                $msgErrors = implode(', ', $errors);
                throw new \Exception("Ошибка парсинга. [$msgErrors]. file=$filePath numLine=$numLine | $line");
            }

            $this->resultDatas[] = $data;
        }
    }

    public function processEnd(int $countTotalLines, string $filePath, int $importId): void
    {
        if (!$this->isSkip) {
            $this->handler->saveDatas($this->resultDatas, $importId);
        }

        $this->resultDatas = [];
    }
}
