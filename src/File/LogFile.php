<?php

namespace app\File;

use app\Xxx\LogFileInterface;

class LogFile implements LogFileInterface
{
    private ?\SplFileObject $file = null;
    private string $filePath;
    private ?int $countLines = null;

    public function __construct(string $filePath)
    {
        $this->filePath = $filePath;
    }

    public function getCountLines(): int
    {
        $file = $this->getFileHandle();

        if ($this->countLines === null) {
            $file->seek(PHP_INT_MAX);
            $this->countLines = $file->key() + 1;
            $file->rewind();
        }

        return $this->countLines;
    }

    public function getLineNumber(): int
    {
        return $this->getFileHandle()->key() + 1;
    }

    public function isEof(): bool
    {
        return $this->getFileHandle()->eof();
    }

    public function readLine(): string
    {
        return $this->getFileHandle()->fgets();
    }

    public function open(bool $validation = true): void
    {
        if ($validation) {
            $error = $this->validate();

            if ($error) {
                throw new \Exception("$error path=$this->filePath");
            }
        }

        $this->file = new \SplFileObject($this->filePath);
        $this->file->setFlags(\SplFileObject::DROP_NEW_LINE | \SplFileObject::READ_AHEAD | \SplFileObject::SKIP_EMPTY);
    }

    public function close(): void
    {
        $this->getFileHandle();
        $this->file = null;
    }

    public function validate(): string
    {
        if (!is_file($this->filePath)) {
            return 'Файл не найден';
        }

        if (!is_readable($this->filePath)) {
            return 'Файл не читается';
        }

        return '';
    }

    public function getFilePath(): string
    {
        return $this->filePath;
    }

    private function getFileHandle(): \SplFileObject
    {
        if (!$this->file) {
            throw new \Exception("Файл не открыт filePath=$this->filePath");
        }

        return $this->file;
    }
}
