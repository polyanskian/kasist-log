<?php

namespace app\Db;

use yii\db\Connection;

class SqliteConnectionFactory
{
    public function createConnection(string $pathFileDb): Connection
    {
        return new Connection([
            'dsn' => "sqlite:$pathFileDb",
            'charset' => 'utf8',
            'enableSchemaCache' => false,
        ]);
    }
}
