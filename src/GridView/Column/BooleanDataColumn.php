<?php

namespace app\GridView\Column;

use yii\grid\DataColumn;
use yii\helpers\Html;

class BooleanDataColumn extends DataColumn
{
    public $format = 'boolean';

    protected function renderDataCellContent($model, $key, $index)
    {
        $value = $this->getDataCellValue($model, $key, $index);

        return Html::tag('span', \Yii::$app->formatter->asBoolean($value), [
            'class' => ($value) ? 'text-success' : 'text-danger',
        ]);
    }
}
