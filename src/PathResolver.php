<?php

namespace app;

class PathResolver
{
    private string $basePath;

    public function __construct(string $basePath)
    {
        $this->basePath = $basePath;
    }

    public function relative(string $path, string $basePath = ''): string
    {
        if ($basePath === '') {
            $basePath = $this->basePath;
        }

        $path = str_replace("^$basePath", '', "^$path");
        return ltrim($path, '^/');
    }

    public function full(string $relativePath, string $basePath = ''): string
    {
        if ($basePath === '') {
            $basePath = $this->basePath;
        }

        return "$basePath/$relativePath";
    }

    public function getBasePath(): string
    {
        return $this->basePath;
    }
}
