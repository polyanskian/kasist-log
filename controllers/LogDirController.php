<?php

declare(strict_types=1);

namespace app\controllers;

use app\Exception\ShowUserException;
use app\models\search\LogDirSearch;
use app\NotifyManager\NotifyManagerInterface;
use app\Service\LogDir\LogDirService;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class LogDirController extends Controller
{
    public const TITLE_INDEX = 'Директории логов';

    private NotifyManagerInterface $notifyManager;
    private LogDirService $dirService;

    public function __construct(
        $id,
        $module,
        LogDirService $dirService,
        NotifyManagerInterface $notifyManager,
        $config = []
    ) {
        $this->notifyManager = $notifyManager;
        parent::__construct($id, $module, $config);
        $this->dirService = $dirService;
    }

    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::class,
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    public function actionIndex(): string
    {
        $searchModel = new LogDirSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = $this->dirService->createModel();

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            $this->notifyManager->success('Создано');
            return $this->redirect(['update', 'id' => $model->id]);
        }

        $model->loadDefaultValues();

        $logDirPaths = $this->dirService->getLogRelativeDirPaths();

        return $this->render('create', [
            'model' => $model,
            'logDirPaths' => $logDirPaths,
        ]);
    }

    public function actionUpdate(int $id)
    {
        try {
            $model = $this->dirService->findModel($id);
        } catch (ShowUserException $e) {
            throw new NotFoundHttpException($e->getMessage());
        }

        if ($this->request->isPost && $model->load($this->request->post())) {
            $model->directory = $model->getOldAttribute('directory');

            if ($model->save()) {
                $this->notifyManager->success('Обновлено');
                return $this->redirect(['update', 'id' => $model->id]);
            }
        }

        $logDirPaths = $this->dirService->getLogRelativeDirPaths();

        return $this->render('update', [
            'model' => $model,
            'logDirPaths' => $logDirPaths,
        ]);
    }

    public function actionDelete(int $id): Response
    {
        try {
            $model = $this->dirService->findModel($id);
            $model->delete();
            $this->notifyManager->success('Директория удалена', [$model->name, $model->directory]);
        } catch (ShowUserException $e) {
            $this->notifyManager->error($e->getMessage(), $e->getDetails());
        }

        return $this->redirect(['index']);
    }
}
