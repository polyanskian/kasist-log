<?php

namespace app\controllers;

use app\Exception\ShowUserException;
use app\models\search\LogImportSearch;
use app\NotifyManager\NotifyManager;
use app\Service\LogDir\LogDirService;
use app\Service\LogImport\Model\FileLogModel;
use app\Service\LogImport\Model\FindLogModel;
use app\Service\LogImport\LogImportService;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\Response;

class LogImportController extends Controller
{
    public const TITLE_INDEX = 'Импорт логов';

    private NotifyManager $notifyManager;
    private LogImportService $importService;
    private LogDirService $dirService;

    public function __construct(
        $id,
        $module,
        LogImportService $importServuce,
        LogDirService $dirService,
        NotifyManager $notifyManager,
        $config = []
    ) {
        $this->notifyManager = $notifyManager;
        parent::__construct($id, $module, $config);
        $this->importService = $importServuce;
        $this->dirService = $dirService;
    }

    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::class,
                    'actions' => [
                        'delete' => ['POST'],
                        'restart' => ['POST'],
                    ],
                ],
            ]
        );
    }

    public function actionIndex(): string
    {
        $searchModel = new LogImportSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $modelShowFile = new FindLogModel();
        $modelFile = new FileLogModel();

        $logDirs = $this->dirService->findAllDirs();

        $logFileNames = [];

        // $modelShowFile->dateFrom = '2022-02-02';
        // $modelShowFile->dateTo = '2022-02-04';

        if ($this->request->isPost) {
            $modelShowFile->load($this->request->post());
            $modelFile->load($this->request->post());

            if ($modelFile->isActionCreate && $modelFile->validate()) {
                try {
                    $importModels = $this->importService->createImports($modelFile, $logDirs);

                    foreach ($importModels as $importModel) {
                        $this->importService->runImport($importModel);
                    }

                    $this->notifyManager->success('Импорт создан', $modelFile->files);
                } catch (ShowUserException $e) {
                    $this->notifyManager->error($e->getMessage(), $e->getDetails());
                }

                return $this->redirect('index');
            }

            if ($modelShowFile->validate()) {
                try {
                    $logFileNames = $this->importService->getImportLogFileNames($modelShowFile);
                } catch (ShowUserException $e) {
                    $this->notifyManager->error($e->getMessage(), $e->getDetails());
                }
            }
        }

        return $this->render('create', [
            'modelShowFile' => $modelShowFile,
            'modelFile' => $modelFile,
            'logDirs' => $logDirs,
            'logFileNames' => $logFileNames,
        ]);
    }

    public function actionRestart(int $id): Response
    {
        try {
            $model = $this->importService->findModel($id);
            $this->importService->restartImport($model);
            $this->notifyManager->success("Импорт перезапущен (id=$model->id $model->file_name)");
        } catch (ShowUserException $e) {
            $this->notifyManager->error($e->getMessage(), $e->getDetails());
        }

        return $this->redirect(['index', 'id' => $model->id]);
    }

    public function actionDelete($id): Response
    {
        try {
            $model = $this->importService->findModel($id);
            $this->importService->deleteModel($model);
            $this->notifyManager->success('Импорт удален', ["id=$model->id $model->file_name"]);
        } catch (ShowUserException $e) {
            $this->notifyManager->error($e->getMessage(), $e->getDetails());
        }

        return $this->redirect(['index']);
    }
}
