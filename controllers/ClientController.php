<?php

declare(strict_types=1);

namespace app\controllers;

use app\Exception\ShowUserException;
use app\models\search\ClientSearch;
use app\NotifyManager\NotifyManagerInterface;
use app\Service\Client\ClientService;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\Response;

class ClientController extends Controller
{
    public const TITLE_INDEX = 'Клиенты';

    private NotifyManagerInterface $notifyManager;
    private ClientService $clientService;

    public function __construct(
        $id,
        $module,
        ClientService $clientService,
        NotifyManagerInterface $notifyManager,
        $config = []
    ) {
        $this->notifyManager = $notifyManager;
        parent::__construct($id, $module, $config);
        $this->clientService = $clientService;
    }

    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::class,
                    'actions' => [
                        'import-start' => ['POST'],
                        'delete-all' => ['POST'],
                    ],
                ],
            ]
        );
    }

    public function actionIndex(): string
    {
        $searchModel = new ClientSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionImportStart(): Response
    {
        try {
            $this->clientService->startImport();
            $this->notifyManager->success('Импорт клиентов запущен');
        } catch (ShowUserException $e) {
            $this->notifyManager->error($e->getMessage());
        }

        return $this->redirect(['index']);
    }

    public function actionDeleteAll(): Response
    {
        try {
            $this->clientService->deleteAllClients();
            $this->notifyManager->success('Клиенты удалены');
        } catch (ShowUserException $e) {
            $this->notifyManager->error($e->getMessage());
        }

        return $this->redirect('index');
    }
}
