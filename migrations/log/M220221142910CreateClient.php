<?php

namespace app\migrations\log;

use app\DbTableName;
use yii\db\Migration;

class M220221142910CreateClient extends Migration
{
    public function safeUp()
    {
        $tableName = DbTableName::CLIENT;

        $this->createTable($tableName, [
            'id' => $this->primaryKey()->unsigned(),
            'login' => $this->string(255)->notNull()->comment('Логин'),
            'name' => $this->string(255)->null()->comment('Компания'),
        ]);

        $this->createIndex("idx_{$tableName}_login", $tableName, 'login', true);
        $this->createIndex("idx_{$tableName}_login_name", $tableName, ['login', 'name'], true);
    }

    public function safeDown()
    {
        $this->dropTable(DbTableName::CLIENT);
    }
}
