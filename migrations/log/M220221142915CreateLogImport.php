<?php

namespace app\migrations\log;

use app\DbTableName;
use yii\db\Migration;

class M220221142915CreateLogImport extends Migration
{
    public function safeUp()
    {
        $tableName = DbTableName::LOG_IMPORT;
        $tableRequestLogDir = DbTableName::LOG_DIRECTORY;

        $this->createTable($tableName, [
            'id' => $this->primaryKey()->unsigned(),
            'log_directory_id' => $this->integer()->unsigned()->notNull(),
            'file_name' => $this->string(255)->notNull()->comment('Имя файла'),
            'date' => $this->date()->notNull()->comment('Дата'),
            'is_completed' => $this->boolean()->notNull()->defaultValue(0)->comment('Завершено'),
            'is_run' => $this->boolean()->notNull()->defaultValue(0)->comment('Выполняется'),
            'date_update' => $this->dateTime()->null()->comment('Дата импорта')
        ]);

        $this->createIndex("idx_{$tableName}_file_directory", $tableName, ['file_name', 'log_directory_id'], true);

        $this->addForeignKey(
            "fk_{$tableName}_directory", $tableName, 'log_directory_id',
            $tableRequestLogDir, 'id', 'CASCADE'
        );
    }

    public function safeDown()
    {
        $tableName = DbTableName::LOG_IMPORT;

        $this->dropForeignKey("fk_{$tableName}_directory", $tableName);
        $this->dropTable($tableName);
    }
}
