<?php

namespace app\migrations\log;

use app\DbTableName;
use yii\db\Migration;

class M220221142914CreateLogDirectory extends Migration
{
    public function safeUp()
    {
        $tableName = DbTableName::LOG_DIRECTORY;

        $this->createTable($tableName, [
            'id' => $this->primaryKey()->unsigned(),
            'name' => $this->string(255)->notNull()->comment('Имя'),
            'directory' => $this->string(255)->notNull()->comment('Директория'),
        ]);

        $this->createIndex("idx_{$tableName}_name", $tableName, 'name', true);
        $this->createIndex("idx_{$tableName}_directory", $tableName, 'directory', true);
    }

    public function safeDown()
    {
        $this->dropTable(DbTableName::LOG_DIRECTORY);
    }
}
