<?php

namespace app\migrations\log;

use app\DbTableName;
use yii\db\Migration;

class M220221142911CreateIp extends Migration
{
    public function safeUp()
    {
        $tableName = DbTableName::IP;

        $this->createTable($tableName, [
            'id' => $this->primaryKey()->unsigned(),
            'ip' => $this->string(16)->notNull(),
        ]);

        $this->createIndex("idx_{$tableName}_ip", $tableName, 'ip', true);
    }

    public function safeDown()
    {
        $this->dropTable(DbTableName::IP);
    }
}
