<?php

namespace app\migrations\log;

use app\DbTableName;
use yii\db\Migration;

class M220221142927CreateRequestSearchq extends Migration
{
    public function safeUp()
    {
        $tableName = DbTableName::REQUEST_SEARCHQ;
        $tableIp = DbTableName::IP;
        $tableImport = DbTableName::LOG_IMPORT;

        $this->createTable($tableName, [
            'id' => $this->primaryKey()->unsigned(),
            'import_id' => $this->integer()->unsigned()->notNull()->comment('Импорт'),
            'ip_id' => $this->integer()->unsigned()->null(),
            'ip_unknown' => $this->string(16)->null()->comment('Неизвестный IP'),
            'soft' => $this->string(255)->null()->comment('Программа'),
            'date' => $this->dateTime()->notNull()->comment('Дата запроса'),
        ]);

        $this->addForeignKey("fk_{$tableName}_ip", $tableName, 'ip_id', $tableIp, 'id', 'CASCADE');

        $this->addForeignKey(
            "fk_{$tableName}_import", $tableName, 'import_id',
            $tableImport, 'id', 'CASCADE'
        );
    }

    public function safeDown()
    {
        $tableName = DbTableName::REQUEST_SEARCHQ;

        $this->dropForeignKey("fk_{$tableName}_ip", $tableName);
        $this->dropForeignKey("fk_{$tableName}_import", $tableName);
        $this->dropTable($tableName);
    }
}
