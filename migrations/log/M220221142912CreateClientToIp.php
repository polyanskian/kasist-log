<?php

namespace app\migrations\log;

use app\DbTableName;
use yii\db\Migration;

class M220221142912CreateClientToIp extends Migration
{
    public function safeUp()
    {
        $tableName = DbTableName::CLIENT_TO_IP;
        $tableClient = DbTableName::CLIENT;
        $tableIp = DbTableName::IP;

        $this->createTable($tableName, [
            'client_id' => $this->integer()->unsigned()->notNull(),
            'ip_id' => $this->integer()->unsigned()->notNull(),
        ]);

        $this->createIndex("idx_{$tableName}_client_ip", $tableName, ['client_id', 'ip_id'], true);

        $this->addForeignKey("fk_{$tableName}_client", $tableName, 'client_id', $tableClient, 'id', 'CASCADE');
        $this->addForeignKey("fk_{$tableName}_ip", $tableName, 'ip_id', $tableIp, 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $tableName = DbTableName::CLIENT_TO_IP;

        $this->dropForeignKey("fk_{$tableName}_client", $tableName);
        $this->dropForeignKey("fk_{$tableName}_ip", $tableName);
        $this->dropTable($tableName);
    }
}
