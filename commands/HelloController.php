<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\LogImport;
use app\PathResolver;
use app\Xxx\Handler\DocInfoNdHandler;
use app\Xxx\Handler\DndHandler;
use app\Xxx\Handler\MainpageSourceHandler;
use app\Xxx\Handler\PersonaldataHandler;
use app\Xxx\Handler\SearchqHandler;
use app\Xxx\PrepareLine\TrimPrepareLine;
use app\Xxx\PrepareLine\UrlDecodePrepareLine;
use app\Xxx\Handler\SearchTextHandler;
use app\Xxx\Handler\ServerInfoHandler;
use app\Xxx\Processor\DocInfoNdProcessor;
use app\Xxx\Processor\PersonaldataProcessor;
use app\Xxx\Processor\SearchqProcessor;
use app\Xxx\Processor\SearchTextProcessor;
use app\Xxx\Processor\ServerInfoProcessor;
use app\Xxx\LogFileImporter;
use app\Xxx\Processor\DndProcessor;
use app\Xxx\Processor\MainpageSourceProcessor;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\Console;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class HelloController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionIndex(): int
    {
        $pathResolver = \Yii::$container->get(PathResolver::class);

//        $importService = \Yii::$container->get(RequestImportService::class);
//
//        $model = $importService->findModel(1);
//        $command = new StartRequestImportCommand($model);
//        $importService->processImport($command);

        $logImport = LogImport::find()
            ->byId(1)
            ->one();

        $fileRelativePath = $logImport->getFileRelativePath();
        $filePath = $pathResolver->full($fileRelativePath);

        $prepareLineHandlers = [
            new UrlDecodePrepareLine(),
            new TrimPrepareLine(),
        ];

        $processors = [
            new ServerInfoProcessor(new ServerInfoHandler()),
            new SearchTextProcessor(new SearchTextHandler()),
            new DocInfoNdProcessor(new DocInfoNdHandler()),
            new MainpageSourceProcessor(new MainpageSourceHandler()),
            new DndProcessor(new DndHandler()),
            new PersonaldataProcessor(new PersonaldataHandler()),
            new SearchqProcessor(new SearchqHandler()),
        ];

//        $collector->run($pathResolver->full('data/logs/server_1/access.2022-01-17.log'));
        $logFileImporter = new LogFileImporter($processors, $prepareLineHandlers);
        $logFileImporter->run($filePath, $logImport->id);

        $this->stdout('[OK]', Console::FG_GREEN);
        $this->stdout("\n");
        return ExitCode::OK;
    }
}
