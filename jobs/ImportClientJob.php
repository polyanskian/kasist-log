<?php

namespace app\jobs;

use app\AppParams;
use app\Service\Client\ClientImportService;
use yii\base\BaseObject;
use yii\queue\JobInterface;

class ImportClientJob extends BaseObject implements JobInterface
{
    public function execute($queue)
    {
        $appParams = \Yii::$container->get(AppParams::class);
        $clientImportService = \Yii::$container->get(ClientImportService::class);

        $dbPaths = $appParams->getDbClientPaths();
        $clientImportService->importClients($dbPaths);

        $dataCompanyPath = $appParams->getDataCompanyPath();
        $clientImportService->importCompanies($dataCompanyPath);
    }
}
