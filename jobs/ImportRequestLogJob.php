<?php

declare(strict_types=1);

namespace app\jobs;

use app\Service\LogImport\LogImportService;
use yii\base\BaseObject;
use yii\queue\JobInterface;

class ImportRequestLogJob extends BaseObject implements JobInterface
{
    public int $importId;

    public function execute($queue)
    {
        $importService = \Yii::$container->get(LogImportService::class);

        $model = $importService->findModel($this->importId);
        $importService->processImport($model);
    }
}
